import java.text.DecimalFormat;
import java.util.*;
import java.io.*;

class BinAdaBoosting{
	
	public static String ft =" ";
	public static double [] ftemp;
	public static double zeta = 1;
	class Error{
		public double err;
		public int ind;
		public char dir;
	}
	
	public static void main (String argv[]){
		@SuppressWarnings("resource")
		Scanner inputFile = new Scanner(System.in);
		String datasetFile = inputFile.next();
		File file = new File(datasetFile);
         
         Scanner input;
		try {
			input = new Scanner(file);
			int T = input.nextInt();
			int n = input.nextInt();
			String line= input.nextLine();
			 line= input.nextLine();
			@SuppressWarnings("resource")
			Scanner linescan = new Scanner (line);
			double[] x = new double[n];
			int[] y = new int[n];
			double[] p = new double [n];
			int i=0;
			while (linescan.hasNext()){
				x[i]= linescan.nextDouble();
				i++;
			}
			linescan.close();
		   line = input.nextLine();
		   linescan = new Scanner (line);
		   i = 0;
		   while (linescan.hasNext()){
				y[i]= linescan.nextInt();
				i++;
			}
		   linescan.close();
		   line = input.nextLine();
		   linescan = new Scanner (line);
		   i = 0;
		   while (linescan.hasNext()){
				p[i]= linescan.nextDouble();
				i++;
			}
		   BinAdaBoosting adaBoosting = new BinAdaBoosting();
		   ftemp = new double [n];
		   for (int k = 0; k< n ;k++)
			   ftemp[k] = 0;
		 for (int j =0; j < T ; j++){
			 System.out.println( "ITERATION - " + (j+1));
			 System.out.println( "~~~~~~~~~~~~~~~");
		 p =   adaBoosting.iterator(n, p, x, y);	
		 }
		 System.out.println( "========================================================================================================");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	public double[] iterator(int n, double[] p, double[] x, int[] y){
		
		Error minErr = calculateErr(y,p,n);
	   double weakClass;
	   if (minErr.ind == (n-1)){
		
		   weakClass = (x[minErr.ind]+ 1) ;
		   }
	   else
	   weakClass = ((double)(x[minErr.ind]+ x[minErr.ind+1])/(double)2) ;
		
	   System.out.println("The selected weak classifier: " + weakClass);
		System.out.println("The error of ht: " + minErr.err );
		
		 if (minErr.dir == 'N'){
		 } else {
		}
			 
		double alpha = ((double)1/(double)2)*Math.log((double)(1-minErr.err)/(double)minErr.err );

		System.out.println("The weight of ht: " + alpha);
		double sumpq = 0 ;
		int h =0;
		if ( minErr.dir == 'N')
			h=1;
		else
			h=-1;
		
		for (int i =0; i <n;i++){
			if (i <= minErr.ind){
				p[i]= (double)p[i]* (Math.pow(Math.E,(-1)*alpha*y[i]*h));  
			}
			else{
				p[i]= (double)p[i]* (Math.pow(Math.E,alpha*y[i]*h));  
			}
			sumpq= sumpq+p[i];
		}
		
		System.out.println("The probabilities normalization factor: "+ sumpq);
		System.out.println("The probabilities after normalization: ");
		for (int i =0; i <n;i++){
			p[i] = round((double)p[i] / (double)sumpq);
			System.out.print( p[i] + "  ");
		}
		System.out.println();
		ft = ft + alpha + " " + "I (x < " + weakClass + ") + ";
		System.out.println ("The boosted classifier: ft ");
		System.out.println (ft.substring(0, ft.length()-2));
		int errors = 0;
		for (int i=0 ; i < n ; i ++){
			if (minErr.dir  == 'N'){
			if (x[i] < weakClass)
			ftemp[i]= ftemp[i] + (alpha * 1) ;
			else 
				ftemp[i]= ftemp[i] + (alpha * -1) ;
			}
			else{
				if (x[i] > weakClass)
					ftemp[i]= ftemp[i] + (alpha * 1) ;
					else 
						ftemp[i]= ftemp[i] + (alpha * -1) ;
					
			}
			
			if ((y[i] > 0 && ftemp[i] > 0)|| (y[i] < 0 && ftemp[i]< 0)   ){
				
			}
			else
				errors = errors + 1;
			
		}
		System.out.println ( "The error of the boosted classifer: "+ (double)errors/(double)n);
		zeta =zeta * sumpq;
		System.out.println ("The bound on Et: " + zeta);
		System.out.println("____________________________________________________________________");
		return p;
	}

	public static	double round(double d){
		DecimalFormat f = new DecimalFormat("#.#####");
		return Double.valueOf(f.format(d));
	}
 	
  public Error calculateErr(int y[], double p[], int n){
	  
	  double error= 0;
	  int index = 0;
	  double minerr = 1;
	  char direction = 'N';
	  
	  double temp;
      for (int i=1 ;i < n; i++){
		  
		  if (y[i] == 1){
			  error = round(p[i] + error);
		  }
		  		  
	  }
	  if (error > 0.5){
		  temp= 1-error;
		  if (temp < minerr){
			  minerr = temp;
			  direction = 'R';
		  }
	  }
	  else{
		  if (error < minerr){
			  minerr = error;
			  direction = 'N';
		  }
	  }

      for (int i= 1; i <n ; i++){
		  if (y[i] == -1){
			  error = round(error + p[i]);
		  }
		  else{
			  error = round(error - p[i]);
		  }
		  if (error > 0.5){
			  temp= 1-error;
			  if (temp < minerr){
				  minerr = temp;
				  direction = 'R';
				  index = i;
			  }
		  }
		  else{
			  if (error < minerr){
				  minerr = error;
				  direction = 'N';
				  index = i;
			  }
		  }
	  }
		  
	  Error e = new Error();
	  e.ind = index;
	  e.dir = direction;
	  e.err =minerr;
	  return e;
  }
	
	
	

}