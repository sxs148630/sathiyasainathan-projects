adaboost-1.dat
ITERATION - 1
~~~~~~~~~~~~~~~
The selected weak classifier: 2.0
The error of ht: 0.25
The weight of ht: 0.5493061443340549
The probabilities normalization factor: 0.8660254037844386
The probabilities after normalization: 
0.16667  0.16667  0.16667  0.5  
The boosted classifier: ft 
 0.5493061443340549 I (x < 2.0) 
The error of the boosted classifer: 0.25
The bound on Et: 0.8660254037844386
____________________________________________________________________
ITERATION - 2
~~~~~~~~~~~~~~~
The selected weak classifier: 5.5
The error of ht: 0.16666000000000003
The weight of ht: 0.8047429566010608
The probabilities normalization factor: 0.7453664278058304
The probabilities after normalization: 
0.50002  0.1  0.1  0.29999  
The boosted classifier: ft 
 0.5493061443340549 I (x < 2.0) + 0.8047429566010608 I (x < 5.5) 
The error of the boosted classifer: 0.25
The bound on Et: 0.6455062616079089
____________________________________________________________________
ITERATION - 3
~~~~~~~~~~~~~~~
The selected weak classifier: 7.5
The error of ht: 0.2
The weight of ht: 0.6931471805599453
The probabilities normalization factor: 0.800005
The probabilities after normalization: 
0.31251  0.25  0.25  0.18749  
The boosted classifier: ft 
 0.5493061443340549 I (x < 2.0) + 0.8047429566010608 I (x < 5.5) + 0.6931471805599453 I (x < 7.5) 
The error of the boosted classifer: 0.0
The bound on Et: 0.5164082368176351
____________________________________________________________________
ITERATION - 4
~~~~~~~~~~~~~~~
The selected weak classifier: 2.0
The error of ht: 0.18749
The weight of ht: 0.733201355582801
The probabilities normalization factor: 0.7806087365639716
The probabilities after normalization: 
0.19231  0.15384  0.15384  0.5  
The boosted classifier: ft 
 0.5493061443340549 I (x < 2.0) + 0.8047429566010608 I (x < 5.5) + 0.6931471805599453 I (x < 7.5) + 0.733201355582801 I (x < 2.0) 
The error of the boosted classifer: 0.0
The bound on Et: 0.4031127812934424
____________________________________________________________________
ITERATION - 5
~~~~~~~~~~~~~~~
The selected weak classifier: 5.5
The error of ht: 0.19232000000000005
The weight of ht: 0.717502644562948
The probabilities normalization factor: 0.788225706013174
The probabilities after normalization: 
0.49999  0.09524  0.09524  0.30954  
The boosted classifier: ft 
 0.5493061443340549 I (x < 2.0) + 0.8047429566010608 I (x < 5.5) + 0.6931471805599453 I (x < 7.5) + 0.733201355582801 I (x < 2.0) + 0.717502644562948 I (x < 5.5) 
The error of the boosted classifer: 0.0
The bound on Et: 0.31774385663795784
____________________________________________________________________
========================================================================================================