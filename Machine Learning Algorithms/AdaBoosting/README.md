Implementation of Binary and Real Adaboosting algorithm

Create a Java package and add the attached java class files inside the package.

BinAdaBoosting.java implements the logic for Binary adaboost algorithm and RealAdaBoosting.java implements the logic for Real adaboost algorithm and the main function which calls the input files and classify the given test data based on the weak classifier and the probability of occurrence of each data.

There are 5 steps to implement this,

1. Create a package
2. Add the java class files BinAdaBoosting.java and RealAdaBoosting.java inside the package
3. Place the input files in a specified working directory
4. Run the program and give the input file directory with the input file names
5. Check the output with the output files attached.