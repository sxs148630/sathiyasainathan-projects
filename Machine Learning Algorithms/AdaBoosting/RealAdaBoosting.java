import java.text.DecimalFormat;
import java.util.*;
import java.io.*;
import java.lang.Math;
class RealAdaBoosting{
	
	public static String f =" ";
	public static double [] ftemp;
	public static double z = 1;

	class Ming{
		public double probs[] = new double [4];
		public double g;
		public char dir;
		public int ind;
	}
	
	public static void main (String argv[]){
		@SuppressWarnings("resource")
		Scanner inputFile = new Scanner(System.in);
		String datasetFile = inputFile.next();
		File file = new File(datasetFile);
         
         Scanner input;
		try {
			input = new Scanner(file);
			int T = input.nextInt();
			int n = input.nextInt();
			double eps = input.nextDouble();

			String line= input.nextLine();
			 line= input.nextLine();
			@SuppressWarnings("resource")
			Scanner lineScan = new Scanner (line);
			double[] x = new double[n];
			int[] y = new int[n];
			ftemp = new double[n];
			for(int l=0;l<n;l++)
				ftemp[l] = 0;
			double[] p = new double [n];
			int i=0;
			while (lineScan.hasNext()){
				x[i]= lineScan.nextDouble();
				i++;
			}
			lineScan.close();
		   line = input.nextLine();
		   lineScan = new Scanner (line);
		   i = 0;
		   while (lineScan.hasNext()){
				y[i]= lineScan.nextInt();
				i++;
			}
		   lineScan.close();
		   line = input.nextLine();
		   lineScan = new Scanner (line);
		   i = 0;
		   while (lineScan.hasNext()){
				p[i]= lineScan.nextDouble();
				i++;
			}
		   RealAdaBoosting adaBoossting = new RealAdaBoosting();
		
		   for (int j =0; j < T ; j++){
			   System.out.println( "Iteration " + (j+1));
			   System.out.println( "~~~~~~~~~~~~~~~");
		   p =   adaBoossting.iterator(n, eps, p, x, y);
		   }
		   System.out.println( "========================================================================================================");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	public double round_3(double n){

		 n = Math.round(n*1000)/1000.0;
		 return n;
	}
	public double[] iterator(int n, double eps, double[] p, double[] x, int[] y){
		Ming m = calcProbs(y,p,n);

		double weakClass;
		   if (m.ind == 0){
			   weakClass = (x[m.ind]- 1) ;
		   }
		   else
			   weakClass = ((double)(x[m.ind]+ x[m.ind-1])/(double)2) ;
			
		System.out.println("The selected weak classifier: " + weakClass);
		System.out.println ("The G error value of ht: " + m.g);
		
		
		double c1 ,c2;
			c1 = (1.0/2.0) * Math.log ((double)(m.probs[0]+eps) / (double)(m.probs[3]+eps) );
			c2 = (1.0/2.0) * Math.log ((double)(m.probs[2]+eps) / (double)(m.probs[1]+eps) );
			//System.out.println("c1=" + c1);
			System.out.println("The weights ct+, ct-: " + c1 + "    " + c2);
			
			
		double sum = 0;
			for ( int i = 0 ; i < n ;i++){
				if (i < m.ind){
					p[i] = p[i] * Math.pow(Math.E,(-1)* c1 *y[i]) ;
				}
				else
					p[i] = p[i] * Math.pow(Math.E,(-1)* c2 * y[i]);
				sum = sum +p[i];
			}
			System.out.println("The probabilities normalization factor: " + sum);
			System.out.println("The probabilities after normalization: "); 
			for ( int i = 0 ; i < n ;i++){
				p[i] = (double)p[i] /(double)sum;
				System.out.print( p[i]+ " ");
			}
			System.out.println();
			int errors = 0;
			System.out.println("The values ft(xi) for each one of the examples: ");
			for (int i=0 ; i < n ; i ++){
			  if (x[i] < weakClass){
				  ftemp[i] = ftemp[i] + c1 ;
			  }
			  else
				  ftemp[i] = ftemp[i] + c2 ;
			  System.out.print(ftemp[i] + "  ");
			
			
			
			if ((y[i] > 0 && ftemp[i] > 0)|| (y[i] < 0 && ftemp[i]< 0)   ){
				
			}
			else
				errors = errors + 1;
			}
			System.out.println();
			System.out.println ( "The error of the boosted classifer: "+ (double)errors/(double)n);
			z =z * sum;
			System.out.println ("The bound on Et: " + z);
			System.out.println("____________________________________________________________________");
			
		return p;
	}

	public static	double round(double d){
		DecimalFormat f = new DecimalFormat("#.####");
		return Double.valueOf(f.format(d));
	}
 	
  public Ming calcProbs(int y[], double p[], int n){
	  

	double g, ming = Double.MAX_VALUE;
	char direction = 'N';
	Ming m = new Ming();	
	for (int i = 1;i<n;i++){
		  double pr1=0;
		  double pr2=0;
		  double pw1=0;
		  double pw2=0;	  
		   for (int j =0; j <n;j++){
			   if (j<i){
				   if (y[j] == -1 )
					   pw2 = pw2 + p[j];
				   else
					   pr1 = pr1 +p[j];
			   }
			   else{
				   if (y[j] == -1)
					   pr2 = pr2 +p[j];
				   else
					   pw1 = pw1 + p[j];
			   }
		   }
		
		   g = Math.sqrt(pr1*pw2) + Math.sqrt(pw1*pr2);
		   
		   direction = 'N';
		   if (g> 0.5){
			   g = 1-g;
			  double temp = pr1;
			  pr1 = pw1;
			  pw1 = temp;
			  temp = pr2;
			  pr2 = pw2;
			  pw2 = temp;
			  direction = 'R';
		   }
		   if (g< ming){
			   ming = g;
			   m.g = g;
			   m.dir = direction;
			   m.ind = i;
			   m.probs[0]=pr1;
			   m.probs[1]= pr2;
			   m.probs[2]=pw1;
			   m.probs[3] = pw2;
			   
		   }
		   
	  }
	  return m;
	  
  }
	
	
	

}