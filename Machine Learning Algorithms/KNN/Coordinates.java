package knncrossvalidation;
//Coordinates Class to store the coordinates of the given matrix
public class Coordinates {
int x;
int y;
String sign;
public Coordinates(int x,int y,String sign) {
	// TODO Auto-generated constructor stub
this.x=x;
this.y=y;
this.sign=sign;
}
public int getX() {
	return x;
}
public void setX(int x) {
	this.x = x;
}
public int getY() {
	return y;
}
public void setY(int y) {
	this.y = y;
}
public String getSign() {
	return sign;
}
public void setSign(String sign) {
	this.sign = sign;
}

}