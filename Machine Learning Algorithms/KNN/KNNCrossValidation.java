package knncrossvalidation;

import java.text.DecimalFormat;
import java.util.*;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

public class KNNCrossValidation {

	/* Main method for knncrossvalidation */
	public static void main(String[] args) {
		 String filePath = args[0];
		 String filePath2 = args[1];
		//Getting the input files from the path specified in arguments
		File file1 = new File(filePath);
		File file2 = new File(filePath2);
		try {
			//Reading both the input files
			Scanner scanner1 = new Scanner(file1);
			Scanner scanner2 = new Scanner(file2);
			//Copying data from the first input file
			int Kfold = Integer.parseInt(scanner1.next());
			int NumEx = Integer.parseInt(scanner1.next());
			int[] ShuffleOne = new int[NumEx];
			int[] ShuffleTwo = new int[NumEx];
			int[] ShuffleThree = new int[NumEx];
			//Mapping the input data and storing
			Map<Integer, int[]> shuffleMap = new LinkedHashMap<Integer, int[]>();
			//Separating the three permutations from 1st input file 
			for (int i = 0; i < NumEx; i++)
				ShuffleOne[i] = Integer.parseInt(scanner1.next());

			for (int i = 0; i < NumEx; i++)
				ShuffleTwo[i] = Integer.parseInt(scanner1.next());

			for (int i = 0; i < NumEx; i++)
				ShuffleThree[i] = Integer.parseInt(scanner1.next());
			// Adding the shuffles in the shuffle map
			shuffleMap.put(0, ShuffleOne);
			shuffleMap.put(1, ShuffleTwo);
			shuffleMap.put(2, ShuffleThree);

			//Copying data from the second input file
			HashMap<Integer, Coordinates> inputdata = new HashMap<Integer, Coordinates>();

			int Noofrows = Integer.parseInt(scanner2.next());
			int Noofcolumns = Integer.parseInt(scanner2.next());
			int SampleCount = 0;
			// Creating an array with the Positive and Negative examples in 2nd file
			for (int i = 0; i < Noofrows; i++) {
				for (int j = 0; j < Noofcolumns; j++) {
					String temp = scanner2.next();
					if (temp.equals("+") || temp.equals("-")) {
						inputdata.put(SampleCount, new Coordinates(i, j, temp));
						SampleCount++;
					}
				}
			}
			//Getting the fold length for CrossValidation
			int Exset1 = NumEx / Kfold;
			int Exset2 = NumEx - Exset1;
			//Initializing 2-D Array list for the CrossValidation
			//Finding distances and comparing
			List<int[][]> distanceArrayFirstTrain = new ArrayList<int[][]>();
			List<int[][]> distanceArraySecondTrain = new ArrayList<int[][]>();
			List<int[][]> indexArrayFirstTrain = new ArrayList<int[][]>();
			List<int[][]> indexArraySecondTrain = new ArrayList<int[][]>();
			//Initializing the Local Variables
			int[][] distance = null;
			int[][] indextrain = null;
			int[][] distanceNext = null;
			int[][] indexNext = null;
			// Loop for iterating the map containing the shuffles.
			// The distance calculation and the sorting happens in this loop
			for (Map.Entry<Integer, int[]> shuffle : shuffleMap.entrySet()) {
				int[] currentShuffle = shuffle.getValue();
				distance = new int[Exset1][Exset2];
				indextrain = new int[Exset1][Exset2];
				distanceNext = new int[Exset2][Exset1];
				indexNext = new int[Exset2][Exset1];
				for (int i = 0; i < Exset1; i++) {
					//Taking each permutation separately
					//First Fold
					Integer temporary = new Integer(currentShuffle[i]);
					int x1 = inputdata.get(temporary).getX();
					int y1 = inputdata.get(temporary).getY();
					//Get the Sign value
					String sign1 = inputdata.get(temporary).getSign();
					int dist;
					int z = 0;
					for (int j = Exset1; j < 9; j++) {
						int x2 = inputdata.get(currentShuffle[j]).getX();
						int y2 = inputdata.get(currentShuffle[j]).getY();
						String sign2 = inputdata.get(currentShuffle[j])
								.getSign();
						//1st-Fold
						//Calculating the distance between the training and testing Coordinates
						dist = ((int) (Math.pow((x2 - x1), 2) + Math.pow(
								(y2 - y1), 2)));
						//Storing the distance in a 2-D Array
						distance[i][z] = dist;
						indextrain[i][z] = currentShuffle[j];
						z++;
					}
				}
				//Sorting the permutation Coordinates based on the distance for first fold
				for (int i = 0; i < Exset1; i++) {
					for (int j = 1; j < Exset2; j++) {
						int curr = distance[i][j];
						int curr2 = indextrain[i][j];
						int z = j - 1;
						while (z >= 0 && distance[i][z] > curr) {
							distance[i][z + 1] = distance[i][z];
							indextrain[i][z + 1] = indextrain[i][z];
							z = z - 1;
							distance[i][z + 1] = curr;
							indextrain[i][z + 1] = curr2;
						}
						//distance[i][z + 1] = curr;
						//indextrain[i][z + 1] = curr2;
					}
				}
				//Storing all the distance and Sorted Coordinates in the 2-D Array List
				distanceArrayFirstTrain.add(distance);
				indexArrayFirstTrain.add(indextrain);

				//Repeating for 2nd-Fold
				int temp = 0;
				for (int i = Exset1; i < 9; i++) {
					Integer temporary = new Integer(ShuffleOne[i]);
					int x1 = inputdata.get(temporary).getX();
					int y1 = inputdata.get(temporary).getY();
					String sign1 = inputdata.get(temporary).getSign();
					int dist;
					for (int j = 0; j < Exset1; j++) {
						int x2 = inputdata.get(currentShuffle[j]).getX();
						int y2 = inputdata.get(currentShuffle[j]).getY();
						String sign2 = inputdata.get(currentShuffle[j])
								.getSign();
						//2nd-Fold
						//Calculating the distance between the training and testing Coordinates
						dist = ((int) (Math.pow((x2 - x1), 2) + Math.pow(
								(y2 - y1), 2)));
						distanceNext[temp][j] = dist;
						indexNext[temp][j] = currentShuffle[j];
					}
					temp++;
				}
				//Sorting the permutation Coordinates based on the distance for second fold
				for (int i = 0; i < Exset2; i++) {
					for (int j = 1; j < Exset1; j++) {
						int curr = distanceNext[i][j];
						int curr2 = indexNext[i][j];
						int z = j - 1;
						while (z >= 0 && distanceNext[i][z] > curr) {
							distanceNext[i][z + 1] = distanceNext[i][z];
							indexNext[i][z + 1] = indexNext[i][z];
							z = z - 1;
							distanceNext[i][z + 1] = curr;
							indexNext[i][z + 1] = curr2;
						}
						//distanceNext[i][z + 1] = curr;
						//indexNext[i][z + 1] = curr2;
					}
				}
				//Storing all the distance and Sorted Coordinates in the 2-D Array List
				distanceArraySecondTrain.add(distanceNext);
				indexArraySecondTrain.add(indexNext);
			}
			//Start of Error Calculation for the CrossValidation
			Map<Integer, Map<Integer, List<String>>> symbolExampleMap = new LinkedHashMap<Integer, Map<Integer, List<String>>>();
			Map<Integer, List<String>> symbolMap = new HashMap<Integer, List<String>>();
			List<String> symbolList = null;
			int temporary = 0;
			int k = 1;
			double nError1 = 0, nError2 = 0, shuffleError = 0;
			//List for error average of each permutation - 'E'
			List<Double> errorCountAverage =null;
			//List of error values for each NN
			Map<Integer, List<Double>> errorValuesPerNN = new HashMap<Integer, List<Double>>();
			//List of Total error average for each NN
			Map<Integer, Double> errorAverageValuesPerNN = new HashMap<Integer, Double>();
			List<Double> totalError = new ArrayList<Double>();
			List<Double> averageList = null;
			//Error calculation up to 4 Nearest Neighbors
			while (k < 5) {
				errorCountAverage =new ArrayList<Double>();
				for (int shuffle = 0; shuffle < 3; shuffle++) {
					nError1=0;
					nError2=0;
					shuffleError=0;
					int[] currentShuffle = shuffleMap.get(shuffle);
					for (int i = 0; i < Exset1; i++) {
						int[][] currentIndexTrain = indexArrayFirstTrain
								.get(shuffle);
						symbolList = new ArrayList<String>();
						for (int j = 0; j < k; j++) {
							temporary = currentIndexTrain[i][j];
							symbolList.add(inputdata.get(temporary).getSign());

						}
						String symbol = getSymbol(symbolList);
						// Comparing the symbols for error calculation
						if (!(symbol.equals(inputdata.get(currentShuffle[i])
								.getSign())))
							nError1++;
					}
					for (int i = 0; i < Exset2; i++) {
						int[][] currentIndexTrain = indexArraySecondTrain
								.get(shuffle);
						symbolList = new ArrayList<String>();
						for (int j = 0; j < k; j++) {
							temporary = currentIndexTrain[i][j];
							symbolList.add(inputdata.get(temporary).getSign());
						}
						String symbol = getSymbol(symbolList);
						int z = i + 4;
						if (!(symbol.equals(inputdata.get(currentShuffle[z])
								.getSign())))
							nError2++;
					}
					//Finding the average error for each permutation
					shuffleError = (nError1 + nError2)/9;
					//Adding the error average for all the 3 permutations
					errorCountAverage.add(shuffleError);
				}
				//Calculating the Total error per permutations
				errorValuesPerNN.put(k, errorCountAverage);
				k++;
			}
			//Invoking the method populateAverage to populate the error average
			populateAverage(errorValuesPerNN, errorAverageValuesPerNN);
			
			//Initializing Hashmap for Variance and Sigma storage 
			Map<Integer, Double> varianceMap = new HashMap<Integer, Double>();
			Map<Integer, Double> sigmaMap = new HashMap<Integer, Double>();
			//Loop to calculate the Variance
			for (int i=1 ; i<5 ; i++){
				List<Double> errorCountAverageList = errorValuesPerNN.get(i);
				double variance = 0.0;
				for(int m=0; m<errorCountAverageList.size();m++)
				{
					//Variance calculation formula
					variance += Math.pow((errorCountAverageList.get(m)-errorAverageValuesPerNN.get(i)), 2)/2;
				}
				//Storing the calculated Variance for each NN 
				varianceMap.put(i, variance);
				//Calculating Sigma from Variance
				double sigma = Math.sqrt(variance);
				//Storing the calculated Sigma for each NN
				sigmaMap.put(i, sigma);
			}
			
			//Start of Sign prediction for the DOTS
			//Reading the 2nd input file again
			Scanner scanner3 = new Scanner(file2);
			//Copying data from the second input file
			List<Coordinates> dotData = new ArrayList<Coordinates>();
			int NoofrowsforDots = Integer.parseInt(scanner3.next());
			int NoofcolumnsforDots = Integer.parseInt(scanner3.next());
			//Separating and storing the DOTS from the 2nd input file 
			for (int i = 0; i < NoofrowsforDots; i++) {
				for (int j = 0; j < NoofcolumnsforDots; j++) {
					String temp = scanner3.next();
					if (temp.equals(".")) {
						dotData.add(new Coordinates(i, j, temp));
					}
				}
			}
			//Distance calculation and comparison for the dot symbol
				distance = new int[11][9];
				indextrain = new int[11][9];
				for (int i = 0; i < dotData.size(); i++) {
					int x1 = dotData.get(i).getX();
					int y1 = dotData.get(i).getY();
					String sign1 = dotData.get(i).getSign();
					int dist;
					int z = 0;
					for (int j = 0; j < 9; j++) {
						int x2 = inputdata.get(j).getX();
						int y2 = inputdata.get(j).getY();
						String sign2 = inputdata.get(j)
								.getSign();
						//Distance Formula
						dist = ((int) (Math.pow((x2 - x1), 2) + Math.pow(
								(y2 - y1), 2)));
						distance[i][z] = dist;
						indextrain[i][z] = j;
						z++;
					}
				}
				//Sorting the Coordinates based on the calculated Distance
				for (int i = 0; i < 11; i++) {
					for (int j = 1; j < 9; j++) {
						int curr = distance[i][j];
						int curr2 = indextrain[i][j];
						int z = j - 1;
						while (z >= 0 && distance[i][z] > curr) {
							distance[i][z + 1] = distance[i][z];
							indextrain[i][z + 1] = indextrain[i][z];
							z = z - 1;
							distance[i][z + 1] = curr;
							indextrain[i][z + 1] = curr2;
						}
					}
				}
				//Predicting the Signs for 5-Nearest Neighbors
					k =1;
					while (k<6) {
						for (int i = 0; i < 11; i++) {
						symbolList = new ArrayList<String>();
						for (int j = 0; j < k; j++) {
							temporary = indextrain[i][j];
							symbolList.add(inputdata.get(temporary).getSign());
						}
						String dotsymbol = getSymbol(symbolList);
						dotData.get(i).setSign(dotsymbol);
						}
						String[][] symbols = new String[4][5];
						for(int m=0; m<inputdata.size(); m++)
						{
							Coordinates coordinate = inputdata.get(m);
							symbols[coordinate.getX()][coordinate.getY()] = coordinate.getSign();
						}
						for(int m=0; m<dotData.size(); m++)
						{
							Coordinates coordinate = dotData.get(m);
							symbols[coordinate.getX()][coordinate.getY()] = coordinate.getSign();
						}
						String fileNametoWrite = "output" + k;
						PrintWriter outputWriter = new PrintWriter(new FileWriter(fileNametoWrite));
						if(k!=5)
							//Writing the output in the file for first 4-NN with error and Sigma
							outputWriter.print("K=" + k +" e=" + new DecimalFormat("#.###").format(errorAverageValuesPerNN.get(k).doubleValue())+ " sigma=" + new DecimalFormat("#.###").format(sigmaMap.get(k).doubleValue()));
						else
							//Writing the output in the file for 5th-NN without error and Sigma
							outputWriter.print("K=" + k);
							outputWriter.println();
						
						//Writing the predicted Signs in the Output File
						for(int m =0;m<4;m++)
						{
							for(int n=0;n<5;n++)
							{
								outputWriter.print(symbols[m][n]);
								outputWriter.print(" ");
							}
							if(m!=3)
							outputWriter.println();
						}
						outputWriter.close();
						k++;
					}
					//Successfully Completed
					System.out.println("2-fold Cross Validation is done for 5-NN Successfully!!!");
		}
//Catch block catches the error and prints it in the Console
		 catch (Exception e) {
			e.printStackTrace();
		}
	}
//End of the main function 

/* Method To Compare and Get the Symbols */	
private static String getSymbol(List<String> symbolList) {
		int plusCount = 0;
		int minusCount = 0;
		for (String symbol : symbolList) {
			if (symbol.equals("+")) {
				plusCount++;
			} else if (symbol.equals("-")) {
				minusCount++;
			}
		}
		if (plusCount > minusCount) {
			return "+";
		} else {
			return "-";
		}
	}

/* Method To Calculate Average */
private static void populateAverage(Map<Integer, List<Double>> errorListPerNN, Map<Integer, Double> errorAveragesPerNN) {
		double errorsum = 0;
		double average = 0;
		for (int i = 1; i <= errorListPerNN.size(); i++) {
			List<Double> currentList = errorListPerNN.get(i);
			errorsum =0;
			for (int j = 0; j < currentList.size(); j++) {
				errorsum = errorsum + currentList.get(j);
			}
			average = errorsum / currentList.size();
			errorAveragesPerNN.put(i, average);
		}
	}
}