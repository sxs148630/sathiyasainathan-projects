Implementation of Nearest Neighbor Algorithm.

Create a Java package and add the java class files inside the package.

KNNCrossValidation.java implements the logic for KNN cross validation and Coordinates.java calls the function for KNNCrossValidation and has the main function which calls the input files and classify the given test data and used Cross-Validation technique to validate the classified data.

There are 5 steps to implement this,

1. Create a package
2. Add the java class files KNNCrossValidation.java and Coordinates.java inside the package
3. Place the input files in a specified working directory
4. Run the program and give the input file directory with the input file names
5. Check the output with the output files attached.