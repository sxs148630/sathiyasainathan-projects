# Sathiyasainathan Soundararajan - Masters in Computer Science - Software Developer#

## About Me ##
I'm a Software Developer II in a private organization for the past year and half. I graduated a Master’s degree in Computer Science from The University of Texas at Dallas in December 2016. I believe that my professional experience in the field of Computer Science, especially in application design, development and problem solving skills coupled with the projects I have been doing during the coursework at UTD have provided me with the Software design and development skills needed to solve advanced algorithmic problems.

## May 2015 - Dec 2015 ##
I worked as a Software Developer Intern at Sabre Corporations, a product-service based company in Texas. I was responsible for developing and implementing the Common Automation Framework (CAF), which automates the software updates and patch releases throughout the vertical. I used Python, UNIX and C++ as the programming languages to implement this automation framework.

## May 2016 - Dec 2016 ##
I worked as an Application Developer Intern for Securus Technologies, a service based company in Texas. As a part of R&D team, I have researched, developed and implemented an automation framework to generate the Swagger documentation for Restful webservice and SoapUI scripts to verify Java API's developed by in-house developers using. This automation effort translated more than 300 webservices to YAML document in approximately 35 minutes which had reduced 100% human effort. This demo gave me a recognition in the company and get appreciation from the Vice President.

## Feb 2012 - Jul 2014 ##
I worked as a Systems Engineer at Infosys Technologies Limited, a product-service based company in India. As a part of Oracle development team I was responsible for developing oracle forms and interfaces using PL/SQL with minimum running time and implementing it for our client. All These practical experiences in Software development, as well as working with a collaborative team, would allow me to contribute effectively in this position.

## What is this repository for? ##

This Repository documents all the individual projects that I have done so far.

* Quick summary of all the Course Projects
* Maintaining Code base