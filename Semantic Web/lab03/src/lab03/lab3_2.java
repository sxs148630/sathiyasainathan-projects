package lab03;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

public class lab3_2 {
	
	
	public static void main(String[] args) throws Exception{
		org.apache.log4j.Logger.getRootLogger(). setLevel(org.apache.log4j.Level.OFF);
		
		String xmlFileName = "Lab3_2_ssoundararajan.xml";
		String rdfFileName = "Monterey.rdf";
		
		//Open the file using inputstream
		InputStream ip = new FileInputStream(new File(rdfFileName));
		
		//Create an empty in-memory model and populate it from the graph
		Model model = ModelFactory.createMemModelMaker().createDefaultModel();
		
		long startTime = System.currentTimeMillis();
		model.read(ip, null);
		long endTime = System.currentTimeMillis();
		float timeTaken = (endTime - startTime)/1000F;
		System.out.println("Total time of "+timeTaken+" seconds taken to load the "+rdfFileName+" file");
		ip.close();
		
		//Create query
		String queryString = "SELECT ?p ?o WHERE {<urn:monterey:#incident1> ?p ?o}";
		
		Query query = QueryFactory.create(queryString);
		
		//Execute the query and obtain results
		QueryExecution qe = QueryExecutionFactory.create(query, model);
		ResultSet rs = qe.execSelect();
		
		OutputStream out = new FileOutputStream(xmlFileName);
		//Output query results
		ResultSetFormatter.outputAsXML(out, rs);
		
		qe.close();
		
	}

}

