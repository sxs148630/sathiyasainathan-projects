package lab01;

import java.io.FileNotFoundException;
import java.io.FileWriter;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.tdb.TDBFactory;
import org.apache.jena.util.FileManager;
import org.apache.jena.vocabulary.VCARD;


	/** 
	 * Lab 1 -  use a TDB persistence model 
	 * and add your FOAF profile to it.
	 */
@SuppressWarnings("unused")
public class Lab1_4d extends Object {
    public static void main (String args[]) throws FileNotFoundException {
	    	org.apache.log4j.Logger.getRootLogger().
	    	setLevel(org.apache.log4j.Level.OFF);
	    	
	    	// Make a TDB-backed dataset
			final String directory = "MyDatabases/";
			final Dataset dataset = TDBFactory.createDataset(directory + "Dataset1");
	    	
			final String personURI = "http://utdallas/SemanticWeb/AtesKeven";
			final String prefix = "Dr.";
			final String fullName = "Keven L. Ates";
			final String title = "Parttime Lecturer";
			final String email = "mailto:atescomp@utdallas.edu";
			final String dateOfBirth = "April 1, 1901";
			
	    	dataset.begin(ReadWrite.WRITE);
	    	try {
	    		
	    		final Model tdb = dataset.getNamedModel("myrdf");
	    		final Property middle = tdb.createProperty(VCARD.getURI(), "Middle");
	    		final Resource myResource = tdb.createResource(personURI)
	    				.addProperty(VCARD.FN, fullName)
	    				.addProperty(VCARD.N, tdb.createResource()
	    					.addProperty(VCARD.Prefix, prefix))
	    				.addProperty(VCARD.BDAY, dateOfBirth)
	    				.addProperty(VCARD.EMAIL, email)
	    				.addProperty(VCARD.TITLE, title);
	    		
	    		FileManager.get()
				.readModel(tdb, "sathiyasainathan_FOAFFriends.rdf");
	    		
	    		dataset.commit();
	    		
	    		final FileWriter xmlWriter = new FileWriter("lab1_4_ssoundararajan.xml");
				final FileWriter ntpWriter = new FileWriter("lab1_4_ssoundararajan.ntp");
				final FileWriter n3Writer = new FileWriter("lab1_4_ssoundararajan.n3");

				tdb.write(xmlWriter, "RDF/XML");
				tdb.write(ntpWriter, "N-TRIPLE");
				tdb.write(n3Writer, "N3");
	    		
	    	} catch (Exception e) {
	    		e.printStackTrace();
	    	} finally {
	    		dataset.end();
	    	}  	  
	    }
	}


