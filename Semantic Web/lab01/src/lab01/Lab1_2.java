package lab01;

import java.io.FileWriter;
import java.io.IOException;

import org.apache.jena.iri.impl.Main;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.VCARD;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.varia.NullAppender;

@SuppressWarnings("unused")
public class Lab1_2 {
	
	private static final Logger logger = LogManager.getLogger(Main.class);
	
    public static void main (String args[]) {
    	
    	org.apache.log4j.BasicConfigurator.configure(new NullAppender());
    	
    	org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.OFF);
     
    	final String personURI = "http://utdallas/SemanticWeb/AtesKeven";
		final String prefix = "Dr.";
		final String fullName = "Keven L. Ates";
		final String title = "Parttime Lecturer";
		final String email = "mailto:atescomp@utdallas.edu";
		final String dateOfBirth = "April 1, 1901";
		
		// create an empty model
		final Model model = ModelFactory.createDefaultModel();
		
		//create custom property
		final Property middle = model.createProperty(VCARD.getURI(), "Middle");
		
		final Resource myResource = model.createResource(personURI)
			.addProperty(VCARD.FN, fullName)
			.addProperty(VCARD.N, model.createResource()
				.addProperty(VCARD.Prefix, prefix))
			.addProperty(VCARD.BDAY, dateOfBirth)
			.addProperty(VCARD.EMAIL, email)
			.addProperty(VCARD.TITLE, title);

		try {
			final FileWriter xmlOutFile = new FileWriter("lab1_2_ssoundararajan.xml");
			final FileWriter ntripleOutFile = new FileWriter("lab1_2_ssoundararajan.ntp");
			final FileWriter n3OutFile = new FileWriter("lab1_2_ssoundararajan.n3");

			model.write(xmlOutFile, "RDF/XML");
			model.write(ntripleOutFile, "N-TRIPLE");
			model.write(n3OutFile, "N3");
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}
}
