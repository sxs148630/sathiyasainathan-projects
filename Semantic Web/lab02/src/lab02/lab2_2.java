package lab02;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.VCARD;

/**
 * In-memory Jena model (the default model)
 *
 * @author Sathiyasainathan
 */
@SuppressWarnings("unused")
public class lab2_2 {
	
	// Resource Properties Method
	public void resourceMovie(){
		// default
		final String URI = "http://utdallas/semclass#";
		final String movieURI = URI + "Movie-";
		final String personURI = URI + "Person-";
		final String directorTitle = "director";

		// Movie 1 definition
		final String movieNameURI = movieURI + "drStrangelove";
		final String movieTitle = "Dr. Strangelove";
		final String movieYear = "1964";

		// Movie 2 definition
		final String movie2NameURI = movieURI + "aClockworkOrange";
		final String movie2Title = "A Clockwork Orange";
		final String movie2Year = "1971";

		// Director 1 definition
		final String directorURI = personURI + "StanleyKubrick";
		final String drGivenName = "Stanley";
		final String drFamilyName = "Kubrick";
		final String drFullName = drGivenName + " " + drFamilyName;

		// create an empty Model
		final Model model = ModelFactory.createDefaultModel();

		// Create movie and person classes
		final Resource movie = model.createResource(movieURI);
		final Resource person = model.createResource(personURI);

		// Create custom properties
		final Property directorProperty = model.createProperty(movieURI, directorTitle);

		// Create the director
		final Resource directorResource = model.createResource(directorURI)
				.addProperty(RDF.type, person)
				.addProperty(VCARD.FN, drFullName)
				.addProperty(VCARD.N, model.createResource()
						.addProperty(VCARD.Given, drGivenName)
						.addProperty(VCARD.Family, drFamilyName))
				.addProperty(VCARD.TITLE, directorTitle);

		// Create movie1
		final Resource strangelove = model.createResource(movieNameURI)
				.addProperty(RDF.type, movie)
				.addProperty(DC.title, movieTitle)
				.addProperty(DC.date, movieYear)
				.addProperty(directorProperty, directorResource);

		// Create movie2
		final Resource clockwork = model.createResource(movie2NameURI)
				.addProperty(RDF.type, movie)
				.addProperty(DC.title, movie2Title)
				.addProperty(DC.date, movie2Year)
				.addProperty(directorProperty, directorResource);
		model.close();
	}
	
	//Main method
	public static void main(final String[] args) {
		org.apache.log4j.Logger.getRootLogger()
			.setLevel(org.apache.log4j.Level.OFF);
		lab2_2 newResource = new lab2_2();
		newResource.resourceMovie();
	}
}
