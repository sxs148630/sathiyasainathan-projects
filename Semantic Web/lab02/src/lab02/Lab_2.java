package lab02;

import java.io.FileWriter;
import java.io.IOException;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.tdb.TDBFactory;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.VCARD;

/**
 * Graph in Jena using Dublin Core and TDB
 *
 * @author Sathiyasainathan
 */
@SuppressWarnings("unused")
public class Lab_2 {

	// GLOBALS**
	
	// Make a TDB-backed dataset
	final String directory = "MyDatabases/";
	final Dataset dataset = TDBFactory.createDataset(directory + "Dataset1");

	// create an empty Model
	final Model model = dataset.getNamedModel("myrdf");

	//Main method
	public static void main(final String[] args) {
		org.apache.log4j.Logger.getRootLogger()
		.setLevel(org.apache.log4j.Level.OFF);
		Lab_2 newCreateResource = new Lab_2();
		newCreateResource.allDefinitions();
		newCreateResource.createTriplesFromResources();
	}

	// Creates the output N-Triple and XML files
	public void createTriplesFromResources(){
		dataset.begin(ReadWrite.WRITE);
		try {
			dataset.commit();

			// Write model to different formats
			final FileWriter xmlOutFile = new FileWriter("Lab2_3_ssoundararajan.xml");
			final FileWriter n3OutFile = new FileWriter("Lab2_3_ssoundararajan.n3");

			model.write(xmlOutFile, "RDF/XML");
			model.write(n3OutFile, "N3");

			xmlOutFile.close();
			n3OutFile.close();
		} catch (final IOException e) {
			e.printStackTrace();
			dataset.end();
			model.close();
		}
	}

	// Contains all Vocabulary definitions
	public void allDefinitions(){

		// Default definitions
		final String URI = "http://utdallas/semclass#";
		final String movieURI = URI + "Movie-";
		final String personURI = URI + "Person-";
		final String bookURI = URI + "Book-";
		final String directorTitle = "director";
		final String authorTitle = "author";

		// Director definition
		final String directorURI = personURI + "StanleyKubrick";
		final String drGivenName = "Stanley";
		final String drFamilyName = "Kubrick";
		final String drFullName = drGivenName + " " + drFamilyName;

		// Author definition
		final String authorURI = personURI + "PeterGeorge";
		final String authorGivenName = "Peter";
		final String authorFamilyName = "George";
		final String authorFullName = authorGivenName + " " + authorFamilyName;

		// Book definition
		final String bookNameURI = bookURI + "redAlert";
		final String bookTitle = "Red Alert";

		// Movie 1 definition
		final String movie1URI = movieURI + "DrStrangelove";
		final String movie1Title = "Dr. Strangelove";
		final String movie1Year = "1964";

		// Movie 2 definition
		final String movie2URI = movieURI + "AClockworkOrange";
		final String movie2Title = "A Clockwork Orange";
		final String movie2Year = "1971";

		// Create classes
		final Resource movie = model.createResource(movieURI);
		final Resource person = model.createResource(personURI);
		final Resource book = model.createResource(bookURI);

		// Create custom properties
		final Property directorProperty = model.createProperty(movieURI, directorTitle);
		final Property adaptationOfProperty = model.createProperty(movieURI, "AdaptationOf");

		// Create the director
		// and add the properties cascading style
		final Resource directorResource = model.createResource(directorURI)
				.addProperty(RDF.type, person)
				.addProperty(VCARD.FN, drFullName)
				.addProperty(VCARD.N, model.createResource()
						.addProperty(VCARD.Given, drGivenName)
						.addProperty(VCARD.Family, drFamilyName))
				.addProperty(VCARD.TITLE, directorTitle);

		// Create author resource
		final Resource authorResource = model.createResource(authorURI)
				.addProperty(RDF.type, person)
				.addProperty(VCARD.FN, authorFullName)
				.addProperty(VCARD.N, model.createResource()
						.addProperty(VCARD.Given, authorGivenName)
						.addProperty(VCARD.Family, authorFamilyName))
				.addProperty(VCARD.TITLE, authorTitle);

		// Create book resource
		final Resource bookResource = model.createResource(bookNameURI)
				.addProperty(RDF.type, book)
				.addProperty(DC.creator, authorResource)
				.addProperty(DC.title, bookTitle);

		// Create movie 1 resource
		final Resource movie1Resource = model.createResource(movie1URI)
				.addProperty(RDF.type, movie)
				.addProperty(DC.title, movie1Title)
				.addProperty(DC.date, movie1Year)
				.addProperty(directorProperty, directorResource)
				.addProperty(adaptationOfProperty, bookResource);

		// Create movie 2 resource
		final Resource movie2Resource = model.createResource(movie2URI)
				.addProperty(RDF.type, movie)
				.addProperty(DC.title, movie2Title)
				.addProperty(DC.date, movie2Year)
				.addProperty(directorProperty, directorResource)
				.addProperty(adaptationOfProperty, bookResource);
	}
}