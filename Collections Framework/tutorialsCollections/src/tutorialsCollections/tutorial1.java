package tutorialsCollections;

import java.util.*;

//ArrayList
public class tutorial1 {
	public static void main(String[] args) {
		String [] items = {"laptop", "desktop", "mobile", "tablet", "apple", "orange"};
		List<String> itemsList = new ArrayList<String>();
		
		for(String x: items)
			itemsList.add(x);
		
		String [] fruits = {"apple", "orange", "banana", "grapes"};
		List<String> fruitsList = new ArrayList<String>();
		
		for(String y: fruits)
			fruitsList.add(y);
		
		//Print list1
		for(int i=0;i<itemsList.size();i++){
			if(i<itemsList.size()-1){
				System.out.printf("%s, ",itemsList.get(i));
			}else{
				System.out.printf("%s ",itemsList.get(i));
			}
		}
		
		System.out.println();
		
		//Print list1
		for(int i=0;i<fruitsList.size();i++){
			if(i<fruitsList.size()-1){
				System.out.printf("%s, ",fruitsList.get(i));
			}else{
				System.out.printf("%s ",fruitsList.get(i));
			}
		}
		
		System.out.println();
		editList(itemsList,fruitsList);
		
		//Print edited list1
		for(int i=0;i<itemsList.size();i++){
			if(i<itemsList.size()-1){
				System.out.printf("%s, ",itemsList.get(i));
			}else{
				System.out.printf("%s ",itemsList.get(i));
			}
		}
	}
	
	//Remove list2 items from list1s	
	public static void editList(Collection<String> l1, Collection<String> l2){
		Iterator<String> it = l1.iterator();
		
		while(it.hasNext())
			if (l2.contains(it.next()))
				it.remove();
	}
}