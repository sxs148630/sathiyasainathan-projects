package tutorialsCollections;

import java.util.*;

//Queue or PriorityQueue
public class tutorial7 {

	public static void main(String[] args) {
		PriorityQueue<String> queue = new PriorityQueue<String>();
		
		queue.offer("first");
		queue.offer("second");
		queue.offer("third");
		queue.offer("fourth");
		
		System.out.printf("%s ", queue);
		
		queue.peek();
		
		System.out.println();
		
		System.out.printf("%s ", queue);
	}
	
}
