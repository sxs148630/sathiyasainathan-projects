package tutorialsCollections;

import java.util.*;

//Use of Collections class
public class tutorial4 {

	public static void main(String[] args) {
		Character[] sample = {'d','e','r','j','l'};
		//Convert array to a list
		List<Character> newList = Arrays.asList(sample);
		System.out.println("List is: ");
		printList(newList);
		
		//reverse the list as it is, without sorting
		Collections.reverse(newList);
		System.out.println("List after reverse: ");
		printList(newList);
		
		//shuffles the list
		Collections.shuffle(newList);
		System.out.println("List after shuffle: ");
		printList(newList);
		
		Character[] newSample = new Character[5];
		List<Character> copyList = Arrays.asList(newSample);
		//copies the list
		Collections.copy(copyList, newList);
		System.out.println("Copied List: ");
		printList(copyList);
		
		//fills the entire list with the passed value without changing the list size
		Collections.fill(copyList, 'X');
		System.out.println("List after filling: ");
		printList(copyList);
	}
	
	private static void printList(List<Character> list){
		for(Character x: list)
			System.out.printf("%s ", x);
		System.out.println();
	}
}
