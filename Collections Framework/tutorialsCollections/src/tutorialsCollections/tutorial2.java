package tutorialsCollections;

import java.util.*;

//LinkedList
public class tutorial2 {

	public static void main(String[] args) {
		String [] items = {"laptop", "desktop", "mobile", "tablet", "apple", "orange"};
		LinkedList<String> itemsList = new LinkedList<String>();
//		List<String> itemsList = new LinkedList<String>();
		
		for(String x: items)
			itemsList.add(x);
		
		String [] fruits = {"apple", "orange", "banana", "grapes"};
		List<String> fruitsList = new LinkedList<String>();
		
		for(String y: fruits)
			fruitsList.add(y);
		
		//Add all the elements in list 2 to list 1
		itemsList.addAll(fruitsList);
		fruitsList=null; //empty the list2
		
		//Prints the list
		printList(itemsList);
		//Removes the item from src to dest
		removeItems(itemsList,2,5);
		//Again print it after removing
		printList(itemsList);
		//Reverse the list using builtin methods
		reverseList(itemsList);
	}
	
	private static void printList(List<String> list){
		for(String x: list)
			System.out.printf("%s ",x);
		System.out.println();
	}
	
	public static void removeItems(List<String> list, int from, int to){
		list.subList(from, to).clear();
	}
	
	public static void reverseList(List<String> list){
		ListIterator<String> liItr = list.listIterator(list.size());
		while(liItr.hasPrevious())
			System.out.printf("%s ", liItr.previous());
	}
}
