package tutorialsCollections;

import java.util.*;

//Convert LinkedList back to an array
public class tutorial3 {
	
	public static void main(String[] args) {
		String[] items = {"Chips","Coke","Burrito","Quesedilla"};
		LinkedList<String> list = new LinkedList<String>(Arrays.asList(items));
		
		list.add("Ham-Burger");
		list.addFirst("Sandwidch");
		
		//Convert back to an array
		items = list.toArray(new String[list.size()]);
		
		for(String x: items)
			System.out.printf("%s ",x);
		
		//sorting
		System.out.println();
		Collections.sort(list);
		System.out.println(list);
		
		//reverse sorting
		Collections.sort(list, Collections.reverseOrder());
		System.out.println(list);
	}

}