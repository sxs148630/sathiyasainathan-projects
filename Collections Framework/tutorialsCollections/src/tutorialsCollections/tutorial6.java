package tutorialsCollections;

import java.util.*;

//Stack data structure usinng collections
public class tutorial6 {
	
	public static void main(String[] args) {
		
		Stack<String> newStack = new Stack<String>();
		newStack.push("bottom");
		printStack(newStack);
		newStack.push("middle");
		printStack(newStack);
		newStack.push("top");
		printStack(newStack);
		
		newStack.pop();
		printStack(newStack);
		newStack.pop();
		printStack(newStack);
		newStack.pop();
		printStack(newStack);
	}
	
	private static void printStack(Stack<String> stack){
		if(stack.isEmpty())
			System.out.println("There is nothing in the stack");
		else
			System.out.printf("%s TOP\n",stack);
	}

}
