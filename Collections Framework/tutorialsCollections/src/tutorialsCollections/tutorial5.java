package tutorialsCollections;

import java.util.*;

//use of addAll method, reverse, sort, sort in reverse order
//Frequency collections
//Disjoint collections
public class tutorial5 {

	public static void main(String[] args) {
		String[] food = {"pizza","snacks","fires","wings","veggies"};
		List<String> list = Arrays.asList(food);
		
		//Add data to newlist
		ArrayList<String> newList = new ArrayList<String>();
		newList.add("piano");
		newList.add("guitar");
		newList.add("keyboard");
		newList.add("drums");
		
		//printout new list
		for(String x: newList)
			System.out.printf("%s ",x);
		
		//add food[] array to newlist
		Collections.addAll(newList, food);
		
		System.out.println();
		
		for(String x: newList)
			System.out.printf("%s ",x);
		
		//reverse it as it is and print it
		Collections.reverse(newList);
		
		System.out.println();
		
		for(String x: newList)
			System.out.printf("%s ",x);
		
		//sort the list and print it
		Collections.sort(newList);
		System.out.println();
		
		for(String x: newList)
			System.out.printf("%s ",x);
		
		//do a reverse sort and print it
		Collections.sort(newList,Collections.reverseOrder());
		System.out.println();
		
		for(String x: newList)
			System.out.printf("%s ",x);
		
		System.out.println();
		
		//frequency collections checks for one list against other list or a term and prints how many times that list or that term appears in the first list
		System.out.println(Collections.frequency(newList, "snacks"));
		System.out.println(Collections.frequency(newList, list));
		
		for(String x: list)
			System.out.printf("%s ",x);
		System.out.println();
		
		//disjoint collections checks for the common item between two collections and return a boolean value
		boolean value = Collections.disjoint(newList, list);
		
		System.out.println(value);
		
		if(value)
			System.out.println("Nothing in Common");
		else
			System.out.println("Some common items are present");
		
	}
}
