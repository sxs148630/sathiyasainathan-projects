<?php

session_start();
$msg1 = "";
if (isset($_POST['email'])) {
  $fname = $_POST['fname'];
  $lname = $_POST['lname'];
  $email = $_POST['email'];
  $membership = $_POST['membership'];
  $password1 = $_POST['password1'];
  $password2 = $_POST['password2'];

  $fname = stripslashes($fname);
  $lname = stripslashes($lname);
  $email = stripslashes($email);
  $membership = stripslashes($membership);
  $password1 = stripslashes($password1);
  $password2 = stripslashes($password2);
  
  $fname = strip_tags($fname);
  $lname = strip_tags($lname);
  $email = strip_tags($email);
  $membership = strip_tags($membership);
  $password2 = strip_tags($password2);
  $password1 = strip_tags($password1);
  $email = mysql_real_escape_string($email);

  if ((!$fname) || (!$lname) || (!$email) || (!$membership) || (!$password1) || (!$password2)) {
    $msg1 = "<p style='text-align: center; padding: 5px; color: brown; font-family: Roboto; font-size: 14px;'>Please fill all fields!</p>";
  }
  elseif ($password1 != $password2) {
    $msg1 = "<p style='text-align: center; padding: 5px; color: brown; font-family: Roboto; font-size: 14px;'>Password does not match!</p>";
  }
  elseif ( !preg_match ("/^[a-zA-Z\s]+$/",$fname))
  {
    $msg1 = "<p style='text-align: center; padding: 5px; color: brown; font-family: Roboto; font-size: 14px;'>Name should have only letters and spaces!</p>";
  }
  elseif ( !preg_match ("/^[a-zA-Z\s]+$/",$lname))
  {
    $msg1 = "<p style='text-align: center; padding: 5px; color: brown; font-family: Roboto; font-size: 14px;'>Name should have only letters and spaces!</p>";
  }
  elseif(!isset($_POST['check'])){
    $msg1 = "Please check the terms and conditions before signing up";
  }
  else {
    // $password1 = md5($password1);
    include_once ("../scripts/connect.php");
    $msg1= "Connected";
    $msg1="";
    try{

      $checkuser = mysql_query("SELECT * FROM users WHERE email_id = '".$email."'");
      if(mysql_num_rows($checkuser)==1){
        $msg1 = "<p style='text-align: center; padding: 5px; color: #865; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>Your profile already exists! Please <a href='login.php' target='_blank'>login</a> to continue.</p></p>";
      }
      else{
      $ins = mysql_query("INSERT INTO users VALUES('','$fname','$lname','$email','$password1',now(),'$membership')");

        $msg1 = "<p style='text-align: center; padding: 5px; color: #865; font-family: Roboto; font-size: 14px;'>Your Profile has been created. Please <a style='font-family: Roboto; color:#999' href='login.php'>login</a> to continue.</p>";
      }
    }
    catch (exception $e){
      $msg1 = "<p style='text-align: center; padding: 5px; color: brown; font-family: Roboto; font-size: 14px;'>Try again!</p>";
    }
  }
}

?>
<!DOCTYPE html>
<html>
  <head>  
    <title>Sign Up</title>
    <link href="../css/signup_style.css" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link href='http://fonts.googleapis.com/css?family=Patua+One|Roboto:300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

  <script type="text/javascript" src="jquery.cycle.all.js"></script>
  
    <link href='http://fonts.googleapis.com/css?family=Patua+One|Roboto:300' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="../img/MA-logo.jpg">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfonts-->
    <link href='http://fonts.googleapis.com/css?family=Lobster|Pacifico:400,700,300|Roboto:400,100,100italic,300,300italic,400italic,500italic,500' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,500,600,700,300' rel='stylesheet' type='text/css'>
    <!--webfonts-->
  </head>
  <body>
    <div id="heading">
    <header>
      <center><table>
        <tr>
          <center><td>
            <a href="../index.php"><img src="../img/create_thumb.png" width="100" height="100"></a>
          </td></center>
          <center><td>
            <h1>
              <a href="../index.php">DOCTOR <span style="color:#468446">FINDER</span></a>
            </h1>
            <a href="../index.php"><small style="color: brown"><b>We <span style="color: green">Care</span> about You!</b></small></a>
          </td></center>
        </tr>
      </table></center>
    </header>
  </div>

  <div id="menu">
    <nav id="menu_content">
      <ul>
        <li><a href="../index.php" class="selected"><b>HOME</b></a></li>
        <!-- <li><a href="pages/products.php"><b>PRODUCTS</b></a></li> -->
        <li><a href="../pages/services.php"><b>SERVICES</b></a></li>
        <!-- <li><a href="pages/buynow.php"><b>BUY NOW</b></a></li> -->
        <li><a href="../pages/contact.php"><b>CONTACT</b></a></li>
        <!-- <li><a href="pages/login.php"><b>LOG IN or REGISTER</b></a></li> -->
      </ul>
    </nav>
    <ul id="user">
      <!-- <li><a href="pages/login.php">LOG IN or REGISTER</a></li> -->
    </ul>
  </div>
      <!--start-login-form-->
        <div class="main">
            <div class="login-head">
              <!-- <h1>Texas Airlines</h1> -->
          </div>
          <div  class="wrap">
              <div class="Regisration">
                <div class="Regisration-head">
                  <h2><span></span>Register</h2>
               </div>
                <form action="signup.php" method="post" autocomplete="off" enctype="multipart/form-data">
                  <input type="text" placeholder="First Name" name="fname">
                  <input type="text" placeholder="Last Name" name="lname">
                  <input type="text" placeholder="Email Address" name="email" >
                <input type="password" placeholder="Password" name="password1"  >
                <input type="password" placeholder="Confirm Password" name="password2" >
                <input type="text" placeholder="Medical Practitioner Id" name="docId" >
                <select id="dropdown" name="speciality">
                    <option>-Select your Speciality-</option>
                    <option value="345">Acupuncturist</option>
                          <option value="132">Allergist</option>
                          <option value="145">Anesthesiologist</option>
                          <option value="346">Audiologist</option>
                          <option value="364">Bariatric Physician</option>
                          <option value="112">Cardiac Electrophysiologist</option>
                          <option value="105">Cardiologist</option>
                          <option value="143">Cardiothoracic Surgeon</option>
                          <option value="156">Chiropractor</option>
                          <option value="133">Colorectal Surgeon</option>
                          <option value="98">Dentist</option>
                          <option value="101">Dermatologist</option>
                          <option value="359">Dietitian</option>
                          <option value="130">Ear, Nose , Throat Doctor</option>
                          <option value="146">Emergency Medicine Physician</option>
                          <option value="127">Endocrinologist</option>
                          <option value="134">Endodontist</option>
                          <option value="102">Family Physician</option>
                          <option value="106">Gastroenterologist</option>
                          <option value="148">Geneticist</option>
                          <option value="144">Hand Surgeon</option>
                          <option value="110">Hematologist</option>
                          <option value="154">Immunologist</option>
                          <option value="114">Infectious Disease Specialist</option>
                          <option value="99">Internist</option>
                          <option value="354">Medical Ethicist</option>
                          <option value="159">Microbiologist</option>
                          <option value="362">Midwife</option>
                          <option value="373">Naturopathic Doctor</option>
                          <option value="107">Nephrologist</option>
                          <option value="128">Neurologist</option>
                          <option value="383">Neuropsychiatrist</option>
                          <option value="113">Neurosurgeon</option>
                          <option value="349">Nurse Practitioner</option>
                          <option value="384">Nutritionist</option>
                          <option value="104">OB-GYN</option>
                          <option value="357">Occupational Medicine Specialist</option>
                          <option value="111">Oncologist</option>
                          <option value="116">Ophthalmologist</option>
                          <option value="157">Optometrist</option>
                          <option value="151">Oral Surgeon</option>
                          <option value="135">Orthodontist</option>
                          <option value="117">Orthopedic Surgeon</option>
                          <option value="139">Pain Management Specialist</option>
                          <option value="118">Pathologist</option>
                          <option value="152">Pediatric Dentist</option>
                          <option value="100">Pediatrician</option>
                          <option value="136">Periodontist</option>
                          <option value="336">Physiatrist</option>
                          <option value="335">Physical Therapist</option>
                          <option value="120">Plastic Surgeon</option>
                          <option value="121">Podiatrist</option>
                          <option value="344">Preventive Medicine Specialist</option>
                          <option value="153">Primary Care Doctor</option>
                          <option value="137">Prosthodontist</option>
                          <option value="122">Psychiatrist</option>
                          <option value="337">Psychologist</option>
                          <option value="299">Psychosomatic Medicine Specialist</option>
                          <option value="352">Psychotherapist</option>
                          <option value="108">Pulmonologist</option>
                          <option value="124">Radiation Oncologist</option>
                          <option value="123">Radiologist</option>
                          <option value="339">Reproductive Endocrinologist</option>
                          <option value="109">Rheumatologist</option>
                          <option value="155">Sleep Medicine Specialist</option>
                          <option value="129">Sports Medicine Specialist</option>
                          <option value="158">Surgeon</option>
                          <option value="343">Travel Medicine Specialist</option>
                          <option value="382">Urgent Care Doctor</option>
                          <option value="408">Urological Surgeon</option>
                          <option value="126">Urologist</option>
                          <option value="142">Vascular Surgeon</option>
                  </select>
                  <textarea name="docAddress" placeholder="Clinic Address (Patients will be intimated with this address)"></textarea>
                  <input type="text" name ="city" id="city" placeholder="Enter the city" />
                  <select id="dropdown" name="state">
                                      <option value="">-Select-</option>
                                      <option value="AL">Alabama</option>
                                      <option value="AK">Alaska</option>
                                      <option value="AZ">Arizona</option>
                                      <option value="AR">Arkansas</option>
                                      <option value="CA">California</option>
                                      <option value="CO">Colorado</option>
                                      <option value="CT">Connecticut</option>
                                      <option value="DE">Delaware</option>
                                      <option value="DC">District Of Columbia</option>
                                      <option value="FL">Florida</option>
                                      <option value="GA">Georgia</option>
                                      <option value="HI">Hawaii</option>
                                      <option value="ID">Idaho</option>
                                      <option value="IL">Illinois</option>
                                      <option value="IN">Indiana</option>
                                      <option value="IA">Iowa</option>
                                      <option value="KS">Kansas</option>
                                      <option value="KY">Kentucky</option>
                                      <option value="LA">Louisiana</option>
                                      <option value="ME">Maine</option>
                                      <option value="MD">Maryland</option>
                                      <option value="MA">Massachusetts</option>
                                      <option value="MI">Michigan</option>
                                      <option value="MN">Minnesota</option>
                                      <option value="MS">Mississippi</option>
                                      <option value="MO">Missouri</option>
                                      <option value="MT">Montana</option>
                                      <option value="NE">Nebraska</option>
                                      <option value="NV">Nevada</option>
                                      <option value="NH">New Hampshire</option>
                                      <option value="NJ">New Jersey</option>
                                      <option value="NM">New Mexico</option>
                                      <option value="NY">New York</option>
                                      <option value="NC">North Carolina</option>
                                      <option value="ND">North Dakota</option>
                                      <option value="OH">Ohio</option>
                                      <option value="OK">Oklahoma</option>
                                      <option value="OR">Oregon</option>
                                      <option value="PA">Pennsylvania</option>
                                      <option value="RI">Rhode Island</option>
                                      <option value="SC">South Carolina</option>
                                      <option value="SD">South Dakota</option>
                                      <option value="TN">Tennessee</option>
                                      <option value="TX">Texas</option>
                                      <option value="UT">Utah</option>
                                      <option value="VT">Vermont</option>
                                      <option value="VA">Virginia</option>
                                      <option value="WA">Washington</option>
                                      <option value="WV">West Virginia</option>
                                      <option value="WI">Wisconsin</option>
                                      <option value="WY">Wyoming</option>
                          </select>
                 <div class="Remember-me">
                <div class="p-container">
                <!-- <label for="checkbox" style="color:#888; font-family: Roboto; font-size:14px;"><input type="checkbox" name="check" checked>I agree to the <a style="color:#ccc; font-family: Roboto; font-size:14px;" href="terms.php">Terms and Conditions</a></label> -->
                <br>
                <div class ="clear"></div>
                <p style="padding-top:10px"><?php echo $msg1; ?></p>
              </div>
                         
                <div class="submit">
                  <input type="submit" value="Sign Me Up >" >
                </div>
                  <div class="clear"> </div>
                </div>
                      
              </form>
          </div>
        <section class="about">
          <p class="about-links">
            <a href="../index.php" target="_parent">Home</a>
            <a href="login.php" target="_parent">Existing User?</a>
          </p>
          <p class="about-author">
            &copy; 2015 <a href="../index.php" target="_blank">MyAuction.com</a> -
            All rights reserved<br>
            Designed by <a href="http://www.sathiyasainathan.com" target="_blank">Sathya</a>
          </p>
        </section>
        <footer>
          <p><a href="http://www.sathiyasainathan.com/">Sathiyasainathan</a> &copy; Copyrights Reserved 2015-2019. </p><br>
          <a href="#">Back to top</a>
        </footer>
  </body>
</html>


