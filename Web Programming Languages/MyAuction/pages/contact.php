<?php 
$your_email ='vmehavardhini@gmail.com';// <<=== update to your email address

session_start();
$errors = '';
$name = '';
$visitor_email = '';
$user_message = '';

if(isset($_POST['submit']))
{	
	$name = $_POST['name'];
	$visitor_email = $_POST['email'];
	$user_message = $_POST['message'];
	///------------Do Validations-------------
	if(empty($name)||empty($visitor_email))
	{
		$errors .= "\n Name and Email are required fields. ";	
	}
	if(IsInjected($visitor_email))
	{
		$errors .= "\n Bad email value!";
	}
	if(empty($_SESSION['6_letters_code'] ) ||
	  strcasecmp($_SESSION['6_letters_code'], $_POST['6_letters_code']) != 0)
	{
	//Note: the captcha code is compared case insensitively.
	//if you want case sensitive match, update the check above to
	// strcmp()
		$errors .= "\n The captcha code does not match!";
	}
	
	if(empty($errors))
	{
		//send the email
		$to = $your_email;
		$subject="New form submission";
		$from = $your_email;
		$ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
		
		$body = "A user  $name submitted the contact form:\n".
		"Name: $name\n".
		"Email: $visitor_email \n".
		"Message: \n ".
		"$user_message\n".
		"IP: $ip\n";	
		
		$headers = "From: $from \r\n";
		$headers .= "Reply-To: $visitor_email \r\n";
		
		mail($to, $subject, $body,$headers);
		
		header('Location: thank-you.html');
	}
}

// Function to validate against any email injection attempts
function IsInjected($str)
{
  $injections = array('(\n+)',
              '(\r+)',
              '(\t+)',
              '(%0A+)',
              '(%0D+)',
              '(%08+)',
              '(%09+)'
              );
  $inject = join('|', $injections);
  $inject = "/$inject/i";
  if(preg_match($inject,$str))
    {
    return true;
  }
  else
    {
    return false;
  }
}
?>
<!DOCTYPE html> 
<html>
<head>
	
	<link href='http://fonts.googleapis.com/css?family=Patua+One|Roboto:300|Montserrat' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="../img/MA-logo.jpg">
	<title>Contact Us</title>
	<!-- define some style elements-->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<!-- <link href='http://fonts.googleapis.com/css?family=Roboto:400,100' rel='stylesheet' type='text/css'> -->
<style>
.err
{
	font-family : Verdana, Helvetica, sans-serif;
	font-size : 12px;
	color: red;
}
.form_content{
	padding-left: 20px;
	padding-top: 50px;
	/*float: right;*/
	align:center;
	width: 100%;
}
.info{
	padding-top: 50px;
	float: left; 
	width: 100%; 
	padding-right: 20px;
}
</style>

<!-- a helper script for vaidating the form-->
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" src="../scripts/gen_validatorv31.js" type="text/javascript"></script>	
</head>

<body>
	<div id="heading">
		<header>
			<a href="index.php">
			<table align="center">
				<tr>
					<td>
						<img src="../img/create_thumb.png" width="100" height="100">
					</td>
					<td>
						<h1>
							DOCTOR <span style="color:#468446">FINDER</span>
						</h1>
						<small style="color: brown"><b>We <span style="color: green">Care</span> about You!</b></small>
					</td>
				</tr>
			</table>
			</a>
		</header>
	</div>

	<div id="menu">
		<nav id="menu_content">
			<ul>
				<li><a href="../index.php"><b>HOME</b></a></li>
				<!-- <li><a href="products.php"><b>PRODUCTS</b></a></li> -->
				<!-- <li><a href="services.php"><b>SERVICES</b></a></li> -->
				<!-- <li><a href="buynow.php"><b>BUY NOW</b></a></li> -->
				<li><a href="login_landing.php"><b>LOG IN or REGISTER</b></a></li>
				<li><a href="contact.php" class="selected"><b>CONTACT</b></a></li>
			</ul>
		</nav>
		<ul id="user">
			<!-- <li><a href="login.php">LOG IN or REGISTER</a></li> -->
		</ul>
	</div>
<?php
if(!empty($errors)){
echo "<p class='err'>".nl2br($errors)."</p>";
}
?>

<div id='contact_form_errorloc' class='err'></div>
<div class="container">
<div class="info">
<p style="font-size:24px;"><strong>Connect with Us</strong></p>
<!-- <p>MyAuction.com is committed to provide superior service at all the times. We welcome any inquiry, questions or comments you might have related to our products or services. Your on-line translation through our site is safe, however, security of your personal information is always our priority, so please do not email confidential information such as username or password.</p>
<p>If you have not received a response within 1 - 2 business days or if you require immediate response, please contact us at 469-688-5087 Monday - Friday Thursday 9:30 AM – 6:30 PM ET. We look forward to assisting you.</p> -->
<!-- <center><p style="text-align:center; font-weight:bold">Monday – Friday 9:30 AM – 6:30 PM CST</p></center> -->
 <br>
 <br /> 
 <div id="contact_type">
		<div id="type1"><center><p><b>ADDRESS</b><br>7220 McCallum Blvd, Texas - 75252</p></center></div>
		<div id="type2"><center><p><b>EMAIL</b><br>sathiyasainathan@gmail.com</p></center></div>
		<div id="type3"><center><p><b>PHONE</b><br>+1 469 688 5087</p></center></div>
	</div>
<div class="form_content">
<form method="POST" name="contact_form" 
action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>"> 
<p>
<label for='name'>Name: </label><br>
<input type="text" class="form-control input-sm" width="500" name="name" value='<?php echo htmlentities($name) ?>'>
</p>
<p>
<label for='email'>Email: </label><br>
<input type="text" name="email" class="form-control input-sm" width="500" value='<?php echo htmlentities($visitor_email) ?>'>
</p>
<p>
<label for='message'>Message:</label> <br>
<textarea name="message" class="form-control" width="500" rows=5 cols=30><?php echo htmlentities($user_message) ?></textarea>
</p>
<p>
<img src="captcha_code_file.php?rand=<?php echo rand(); ?>" id='captchaimg' ><br>
<label for='message'>Enter the code above here :</label><br>
<input id="6_letters_code" class="form-control input-sm" width="500" name="6_letters_code" type="text"><br>
<small>Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh</small>
</p>
<center><input type="submit" value="SEND" width="100" class="btn" name='submit'></center>
</form>
</div>
</div>
<script language="JavaScript">

var frmvalidator  = new Validator("contact_form");
//remove the following two lines if you like error message box popups
frmvalidator.EnableOnPageErrorDisplaySingleBox();
frmvalidator.EnableMsgsTogether();

frmvalidator.addValidation("name","req","Please provide your name"); 
frmvalidator.addValidation("email","req","Please provide your email"); 
frmvalidator.addValidation("email","email","Please enter a valid email address"); 
</script>
<script language='JavaScript' type='text/javascript'>
function refreshCaptcha()
{
	var img = document.images['captchaimg'];
	img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
}
</script>
</body>
</html>