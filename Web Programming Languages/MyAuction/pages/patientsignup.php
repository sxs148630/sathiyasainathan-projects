<?php

session_start();
$msg1 = "";
if (isset($_POST['email'])) {
  $fname = $_POST['fname'];
  $lname = $_POST['lname'];
  $email = $_POST['email'];
  $password1 = $_POST['password1'];
  $password2 = $_POST['password2'];
  $dob = $_POST['dob'];
  $sex = $_POST['sex'];

  $fname = stripslashes($fname);
  $lname = stripslashes($lname);
  $email = stripslashes($email);
  $password1 = stripslashes($password1);
  $password2 = stripslashes($password2);
  
  $fname = strip_tags($fname);
  $lname = strip_tags($lname);
  $email = strip_tags($email);
  $password2 = strip_tags($password2);
  $password1 = strip_tags($password1);
  $email = mysql_real_escape_string($email);

  if ((!$fname) || (!$lname) || (!$email) || (!$sex) || (!$dob) || (!$password1) || (!$password2)) {
    $msg1 = "<p style='text-align: center; padding: 5px; color: brown; font-family: Roboto; font-size: 14px;'>Please fill all fields!</p>";
  }
  elseif ($password1 != $password2) {
    $msg1 = "<p style='text-align: center; padding: 5px; color: brown; font-family: Roboto; font-size: 14px;'>Password does not match!</p>";
  }
  elseif ( !preg_match ("/^[a-zA-Z\s]+$/",$fname))
  {
    $msg1 = "<p style='text-align: center; padding: 5px; color: brown; font-family: Roboto; font-size: 14px;'>Name should have only letters and spaces!</p>";
  }
  elseif ( !preg_match ("/^[a-zA-Z\s]+$/",$lname))
  {
    $msg1 = "<p style='text-align: center; padding: 5px; color: brown; font-family: Roboto; font-size: 14px;'>Name should have only letters and spaces!</p>";
  }
  else {
    $password3 = md5(md5($password1));
    include_once ("../scripts/connect.php");
    $msg1= "Connected";
    $msg1="";
    try{

      $checkuser = mysql_query("SELECT * FROM users WHERE email_id = '".$email."'");
      if($checkuser){
        $msg1 = "<p style='text-align: center; padding: 5px; color: #865; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>Your profile already exists! Please <a href='login.php' target='_blank'>login</a> to continue.</p></p>";
      }
      else{
        $type = 'P';
      $ins = mysql_query("INSERT INTO users VALUES('$email','$fname','$lname','$password3','$type')");

        $msg1 = "<p style='text-align: center; padding: 5px; color: #865; font-family: Roboto; font-size: 14px;'>Your Profile has been created. Please <a style='font-family: Roboto; color:#999' href='login.php'>login</a> to continue.</p>";
      }
    }
    catch (exception $e){
      $msg1 = "<p style='text-align: center; padding: 5px; color: brown; font-family: Roboto; font-size: 14px;'>Something went wrong, Try again!</p>";
    }
  }
}

?>
<!DOCTYPE html>
<html>
  <head>  
    <title>Sign Up</title>
    <link href="../css/signup_style.css" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link href='http://fonts.googleapis.com/css?family=Patua+One|Roboto:300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

  <script type="text/javascript" src="jquery.cycle.all.js"></script>
  
    <link href='http://fonts.googleapis.com/css?family=Patua+One|Roboto:300' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="../img/MA-logo.jpg">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfonts-->
    <link href='http://fonts.googleapis.com/css?family=Lobster|Pacifico:400,700,300|Roboto:400,100,100italic,300,300italic,400italic,500italic,500' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,500,600,700,300' rel='stylesheet' type='text/css'>
    <!--webfonts-->
  </head>
  <body>
    <div id="heading">
    <header>
      <center><table>
        <tr>
          <center><td>
          </td></center>
          <center><td>
            <h1>
              <a href="../index.php">DOCTOR <span style="color:#468446">FINDER</span></a>
            </h1>
            &nbsp; &nbsp;<a href="../index.php"><small style="color: brown"><b>We <span style="color: green">Care</span> about You!</b></small></a>
          </td></center>
        </tr>
      </table></center>
    </header>
  </div>

        <!--start-login-form-->
        <aside style="padding:10px;float:right; font-family:'Montserrat';font-size:14px;font-weight:bold;"><a href="doctorsignup.php"><p>Are You a Doctor? <span style="color:green">Sign up here</span></p></a></aside>
        <div class="main">
            <div class="login-head">
              <!-- <h1>Texas Airlines</h1> -->
          </div>
          <div  class="wrap">
              <div class="Regisration">
                <div class="Regisration-head">
                  <h2><span></span>Patient&apos;s Register</h2>
               </div>
                <form action="patientsignup.php" method="post" enctype="multipart/form-data">
                  <input type="email" placeholder="Email Address" name="email" >
                  <input type="password" placeholder="Password" name="password1"  >
                <input type="password" placeholder="Confirm Password" name="password2" >
                  <input type="text" placeholder="First Name" name="fname">
                  <input type="text" placeholder="Last Name" name="lname"><br>
                <input type="date" name="dob"><br>
                <select id="dropdown" name="sex">
                    <option>-Gender-</option>
                    <option value="male">Male</option>
                          <option value="female">Female</option>
                        </select>
                 <div class="Remember-me">
                <div class="p-container">
                <!-- <label for="checkbox" style="color:#888; font-family: Roboto; font-size:14px;"><input type="checkbox" name="check" checked>I agree to the <a style="color:#ccc; font-family: Roboto; font-size:14px;" href="terms.php">Terms and Conditions</a></label> -->
                <br>
                <div class ="clear"></div>
                <p style="padding-top:10px"><?php echo $msg1; ?></p>
              </div>
                         
                <div class="submit">
                  <input type="submit" value="Sign Me Up >>" >
                </div>
                  <div class="clear"> </div>
                </div>
                      
              </form>
          </div>
        <section class="about">
          <p class="about-links">
            <a href="../index.php" target="_parent">Home</a>
            <a href="login.php" target="_parent">Existing User?</a>
          </p>
          <p class="about-author">
            &copy; 2015 <a href="../index.php" target="_blank">MyAuction.com</a> -
            All rights reserved<br>
            Designed by <a href="http://www.sathiyasainathan.com" target="_blank">Sathya</a>
          </p>
        </section>
        <footer>
          <p><a href="http://www.sathiyasainathan.com/">Sathiyasainathan</a> &copy; Copyrights Reserved 2015-2019. </p><br>
          <a href="#">Back to top</a>
        </footer>
  </body>
</html>


