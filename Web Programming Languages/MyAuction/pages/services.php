<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link href='http://fonts.googleapis.com/css?family=Patua+One|Roboto:300' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="../img/MA-logo.jpg">
	<title>Services</title>
</head>
<body>

	<div id="heading">
		<header>
			<a href="../index.php">
			<table align="center">
				<tr>
					<td>
						<img src="../img/MA-logo.jpg" width="100" height="100">
					</td>
					<td>
						<h1>
							MyAuction.com
						</h1>
						<small style="color: brown"><b>An <span style="color: green">Open</span> cart Store!</b></small>
					</td>
				</tr>
			</table>
			</a>
		</header>
	</div>

	<div id="menu">
		<nav id="menu_content">
			<ul>
				<li><a href="../index.php"><b>HOME</b></a></li>
				<li><a href="products.php"><b>PRODUCTS</b></a></li>
				<li><a href="services.php" class="selected"><b>SERVICES</b></a></li>
				<li><a href="buynow.php"><b>BUY NOW</b></a></li>
				<li><a href="contact.php"><b>CONTACT</b></a></li>
			</ul>
		</nav>
		<ul id="user">
			<li><a href="login.php">LOG IN or REGISTER</a></li>
		</ul>
	</div>

	<div>
		
	</div>

</body>
</html>