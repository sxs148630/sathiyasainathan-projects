<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link href='http://fonts.googleapis.com/css?family=Patua+One|Roboto:300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="../img/MA-logo.jpg">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<title>Doctor Finder</title>
</head>
<body>

	<div id="heading">
		<header>
			<a href="../index.php">
			<table align="center">
				<tr>
					<td>
						<img src="../img/create_thumb.png" width="100" height="100">
					</td>
					<td>
						<h1>
							DOCTOR <span style="color:#468446">FINDER</span>
						</h1>
						<small style="color: brown"><b>We <span style="color: green">Care</span> about You!</b></small>
					</td>
				</tr>
			</table>
			</a>
		</header>
	</div>

	<div id="menu">
		<nav id="menu_content">
			<ul>
				<li><a href="../index.php"><b>HOME</b></a></li>
				<li><a href="login_landing.php" class="selected"><b>REGISTER</b></a></li>
				<li><a href="contact.php"><b>CONTACT</b></a></li>
			</ul>
		</nav>
		<ul id="user">
			<li><a href="login.php">Already a Member? Log In</a></li>
		</ul>
	</div>
	<section>
		<div style="float:left; padding:20px; width:100%">
		<table id="signbutton" align="center">
			<tr>
				<td style="padding-right:100px;"><p style="font-weight:bold;">I'm a new patient.<p><p>Find a doctor near your locality and <br>book an appointment online for free.</p><br>
					<a href="patientsignup.php"><button>Sign up >></button></a></td>
				<td style="padding-left:100px;"><p style="font-weight:bold;">I'm a Doctor.</p><p>Doctor Finder helps new patients find you and<br>book an appointment based on your availability.</p><br>
					<a href="doctorsignup.php"><button>Sign up >></button></a></td>
			</tr>
		</table>
	</div>
	</section>
	<br>

	<footer>
          <center><p><a href="http://www.sathiyasainathan.com/">Sathiyasainathan</a> &copy; Copyrights Reserved 2015-2019. </p><br></center>
          <a href="#">Back to top</a>
        </footer>
</body>
</html>