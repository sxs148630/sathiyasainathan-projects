<?php

session_start();
$msg = "";
if (isset($_POST['email'])) {
  $email = $_POST['email'];
  $password = $_POST['password'];
  $email = stripslashes($email);
  $password = stripslashes($password);
  $email = strip_tags($email);
  $password = strip_tags($password);
  if ((!$email) || (!$password)) {
    $msg = "<p style='text-align: center; padding: 5px; color: #f00; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>Please fill all fields!</p>";
  }else {
    $email = mysql_real_escape_string($email);
    $password = md5(md5($password));
    include_once ("../scripts/connect.php");
    $sql = mysql_query("SELECT * FROM users WHERE email='$email' AND password='$password'");
    $count = mysql_num_rows($sql);
    if ($count > 0) {
      while ($row = mysql_fetch_array($sql)) {
        $s_id = $row['id'];
        $s_name = $row['email'];
        $s_pass = $row['password'];
        $_SESSION['id'] = $s_id;
        $_SESSION['name'] = $s_name;
        $_SESSION['password'] = $s_pass;
        mysql_query("UPDATE users SET last_login=now() WHERE email='$s_name' LIMIT 1");
        header("location: ../index.php");
      }
    }else {
      $msg = "<p style='text-align: center; padding: 5px; color: #f00; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>Wrong username or password!</p>";
    }
  }
}

?>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>User Login</title>
  <link rel="stylesheet" href="../css/login_style.css">
  <link href='http://fonts.googleapis.com/css?family=Patua+One|Roboto:300' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" href="../img/MA-logo.jpg">
</head>
<body>
  <form method="post" action="login.php" class="login" autocomplete="off">
    <p>
      <label for="login">Username:</label>
      <input type="email" name="email" id="login" placeholder="name@example.com">
    </p>

    <p>
      <label for="password">Password:</label>
      <input type="password" name="password" id="password" placeholder="*********">
    </p>

    <p class="login-submit">
      <button type="submit" name="submit" class="login-button">Login</button>
    </p>

    <p class="forgot-password"><a href="#">Forgot your password?</a></p>
    <p><?php echo $msg; ?></p>
  </form>

  <section class="about">
    <p class="about-links">
      <a href="../index.php" target="_parent">Home</a>
      <a href="login_landing.php" target="_parent">New User?</a>
    </p>
    <p class="about-author">
      &copy; 2015 <a href="../index.php" target="_blank">Doctor Finder</a> -
      All rights reserved<br>
      Designed by <a href="http://www.sathiyasainathan.com" target="_blank">Sathya</a>
    </p>
  </section>
</body>
</html>
