<?php

include_once("scripts/connect.php");

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href='http://fonts.googleapis.com/css?family=Patua+One|Roboto:300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="img/MA-logo.jpg">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

	<script type="text/javascript" src="jquery.cycle.all.js"></script>
	<script type="text/javascript">

	$(function(){

		$('#play').click(function(){
		$('#slider').cycle('resume');
		return false;

	});

		$('#pause').click(function(){
		$('#slider').cycle('pause');
		return false;
	});

		$('#slider').cycle({

			fx:  'scrollHorz',
			next: '#next',
			prev: '#prev',
			pager: '#pager',
			timeout: 2000,
			speed: 3000

		});
});
	</script>
	<title>Doctor Finder</title>

		<style type="text/css">
		#hero{
			width: 100%;
			height: 500px;
			display: block;
			position: relative;
			margin-left: auto;
			margin-right: auto;
		}

		#slider{
			width: 100%;
			height: 500px;
			display: block;
			position: absolute;
			overflow: hidden;
		}

		.info{
			width: 75%;
			height: 500px;
			display: inline-block;
			position: relative;
			bottom: 300px;
			left: 400px;
			font-size: 20px;
			/*line-height: 26px;*/
			/*background: rgba(102, 204, 204, .2);*/
			z-index: 99;
			text-align: center;
		}

		.info button {
			text-align: center;
			padding: 8px;
			background: #468446;
			font-family: 'Montserrat',sans-serif;
			cursor: pointer;
		}

		.info h2{
			font-family: 'Montserrat',sans-serif;
			font-size:20px;
			
			color:#000;

			/*font-weight: bold;*/
			/*color: #fff;*/
			/*padding: 15px 0 0 20px;*/
		}

		.info p{
			font-family: sans-serif;
			font-size: 20px;
			font-weight: lighter;
			color: #000;
			padding: 0 0 0 20px;
			line-height: 1px;
		}

		#play{
			width: 40px;
			height: 40px;
			line-height: 40px;
			text-align: center;
			display: block;
			position: absolute;
			top: 0;
			right: 60px;
			background: rgba(102, 204, 0, .2);
			color: #fff;
			cursor: pointer;
			z-index: 999;
			transform:rotate(90deg);
			-webkit-transform:rotate(90deg);
		}

		#pause{
			width: 40px;
			height: 40px;
			line-height: 40px;
			text-align: center;
			display: block;
			position: absolute;
			top: 0;
			right: 105px;
			background: rgba(102, 204, 0, .2);
			color: #fff;
			cursor: pointer;
			z-index: 999;
			transform:rotate(90deg);
			-webkit-transform:rotate(90deg);
		}


		#next{
			width: 40px;
			height: 40px;
			line-height: 40px;
			text-align: center;
			display: block;
			position: absolute;
			top: 0;
			right: 15px;
			background: rgba(102, 204, 0, .2);
			color: #fff;
			cursor: pointer;
			z-index: 999;
		}

		#prev{
			width: 40px;
			height: 40px;
			line-height: 40px;
			text-align: center;
			display: block;
			position: absolute;
			top: 0;
			right: 150px;
			background: rgba(102, 204, 0, .2);
			color: #fff;
			cursor: pointer;
			z-index: 999;
		}

		#pager{
			width: 100%;
			text-align: center;
			display: block;
			position: absolute;
			cursor: pointer;
			z-index: 999;
		}

		#pager a{
			font-size: .0em;
			width: 10px;
			height: 10px;
			display: inline-block;
			position: relative;
			border: 1px solid #fff;
			border-radius: 20px;
			background: transparent;
			margin: 10px;
		}

		#pager a.activeSlide {
			background: #fff;
		}

		.short_desc{
			/*background: #e5e5e5;*/
			width: 960px;
			height: 100px;
			margin-top: 0px;
			margin-right: auto;
			margin-left: auto;
		}
		.short_desc table{
			width: 100%;
			position: relative;
			top: 20%;
		}
		.short_desc td{
			text-align: left;
			cursor: pointer;
		}
		.short_desc td:nth-child(odd){
			text-align: right;
		}
		.short_desc tr:nth-child(even){
			/*text-align: left;*/
		}
		.short_desc img{
			border-radius: 20%;
		}
		.featured{
			width: 960px;
			height: 450px;
			/*background: #fcfcfc;*/
			margin-left: auto;
			margin-right: auto;
			text-align: center;
			padding-bottom: 25px;
		}
		.featured h2{
			padding-top: 30px;
		}
		.featured div{
			float: left;
			margin-right: 1%;
			margin-left: 1%;
			cursor: pointer;
		}
		.contain{
			width: 1317;
			height:1000px;
			margin-left: auto;
			margin-right: auto;
		}
		.post{
			width:317px;
			height: 950px;
			background: #999;
			float: left;
			color: white;
		}
		.post h3{
			text-align: center;
		}
		.post hr{
			width: 80%;
			margin-top: 15px;
		}
		.post p{
			padding-left: 10px;
			padding-right: 10px;
			text-align: justify;
		}
		.products{
			width:1000px;
			height: 950px;
			/*background: #fcfcfc;*/
			float: left;
		}
		.products div{
			margin-left: 2%;
			margin-right: 2%;
			float: left;
			margin-bottom: 4%;
			cursor: pointer;
		}
		.products h5{
			text-align: left;
		}
		
		.store_info{
			width: 960px;
			height: 300px;
			overflow: hidden;
			margin-left: auto;
			margin-right: auto;
		}
		.store_info h5, h3{
			text-align: left;
		}
		.store_info h5{
			/*float: left;*/
		}
		.store_info div{
			width: 46%;
			float: left;
			padding-top: 20px;
		}
		.store_info 
		.store_info img{
			position: relative;
			top: 30%;
		}
		.store_info .heading a{
			background: lightgrey;
			text-decoration: none;
			padding-right: 5px;
			padding-left: 5px;
			color: white;
			cursor: pointer;
		}

		.footer{
			background: #ccc;
			width: 960px;
			height: 50px;
			margin-top: 10px;
			margin-right: auto;
			margin-left: auto;
			text-align: center;
		}

	</style>
</head>
<body>

	<div id="heading">
		<header>
			<a href="index.php">
			<table align="center">
				<tr>
					<td>
						<img src="img/create_thumb.png" width="100" height="100">
					</td>
					<td>
						<h1>
							DOCTOR <span style="color:#468446">FINDER</span>
						</h1>
						<small style="color: brown"><b>We <span style="color: green">Care</span> about You!</b></small>
					</td>
				</tr>
			</table>
			</a>
		</header>
	</div>

	<div id="menu">
		<nav id="menu_content">
			<ul>
				<li><a href="index.php" class="selected"><b>HOME</b></a></li>
				<!-- <li><a href="pages/products.php"><b>PRODUCTS</b></a></li> -->
				<!-- <li><a href="pages/services.php"><b>SERVICES</b></a></li> -->
				<!-- <li><a href="pages/buynow.php"><b>BUY NOW</b></a></li> -->
				<li><a href="pages/login_landing.php"><b>REGISTER</b></a></li>
				<li><a href="pages/contact.php"><b>CONTACT</b></a></li>
			</ul>
		</nav>
		<ul id="user">
			<li><a href="pages/login.php">Already a Member? Log In</a></li>
		</ul>
	</div>

	<div id="hero">

	<div id="pager"></div>
	<!-- <div id="pause">&asymp;</div>
	<div id="play">&Delta;</div>
	<div id="next">&rang;</div>
	<div id="prev">&lang;</div> -->



	<div id="slider">
		<div class="items">
			<img src="img/banner/1.jpg" width="1317" height="500" alt="" />

				<div class="info">
					<h2>Searching for a doctor?</h2>
					<p><a href="pages/patientsignup.php"><button>Sign up today</button></a></p>
				</div> 

		</div>	<!-- items-->

		<!-- <div class="items">
			<img src="img/banner/2.jpg" width="1317" height="500" alt="" />

				<div class="info">
					<h2>PLACE YOUR BID</h2>
					<p>IT'S ALWAYS EASY TO GET THINGS DONE HERE! <a href="#"><button>Know More</button></a></p>
				</div> 

		</div> -->	<!-- items-->

		<div class="items">
			<img src="img/banner/3.jpg" width="1317" height="500" alt="" />

				<div class="info">
					<h2>Are you a doctor?</h2>
					<p><a href="pages/doctorsignup.php"><button>Sign up today</button></a></p>
				</div> 

		</div>	<!-- items-->

		<div class="items">
			<img src="img/banner/4.jpg" width="1317" height="500" alt="" />

				<div class="info">
					<h2>Search based on specializations</h2>
					<p><a href="pages/service.php"><button>Know More</button></a></p>
				</div> 

		</div>	<!-- items-->

		<!-- <div class="items">
			<img src="img/home/placebid.jpg" width="1317" height="500" alt="" />

				<div class="info">
					<h2>PLACE YOUR BID</h2>
					<p>IT'S ALWAYS EASY TO GET THINGS DONE HERE! <a href="#"><button>Know More</button></a></p>
				</div> 

		</div> -->	<!-- items-->


	</div> <!-- slider-->

</div>	<!-- hero-->

<!-- <div class="short_desc">
	
	<table>
		<tr>
			<td><img src="http://placehold.it/50x50"></td>
			<td>Text 1</td>
			<td><img src="http://placehold.it/50x50"></td>
			<td>Text 2</td>
			<td><img src="http://placehold.it/50x50"></td>
			<td>Text 3</td>
		</tr>
	</table>
</div>

<div class="featured">
	<h2>FEATURED</h2>
	<div class="featured_items"><img src="img/home/bid.jpg" width="300" height="300"><h4>Bidding Makes it Better</h4></div>
	<div class="featured_items"><img src="img/home/buyandsell.jpg" width="300" height="300"><h4>Buy or Sell</h4></div>
	<div class="featured_items"><img src="img/home/placebid.jpg" width="300" height="300"><h4>Make it Online</h4></div>
</div>



<div class="contain">
	<div class="post">
		<h3>POSTS FROM FB</h3>
		<hr>
		<h3>Heading 1</h3>
		<p>Improve him believe opinion offered met and end cheered forbade. Friendly as stronger speedily by recurred. Son interest wandered sir addition end say. Manners beloved affixed picture men ask. Explain few led parties attacks picture company.</p>
		<hr>
		<h3>Heading 2</h3>
		<p>Improve him believe opinion offered met and end cheered forbade. Friendly as stronger speedily by recurred. Son interest wandered sir addition end say. Manners beloved affixed picture men ask. Explain few led parties attacks picture company.</p>
		<hr>
		<h3>Heading 3</h3>
		<p>Improve him believe opinion offered met and end cheered forbade. Friendly as stronger speedily by recurred. Son interest wandered sir addition end say. Manners beloved affixed picture men ask. Explain few led parties attacks picture company.</p>
	</div>

	<div class="products">
		<h3 style="text-align:center;">PRODUCTS</h3>
		<div><h5>Heading 1</h5><img src="http://placehold.it/200x200"></div>
		<div><h5>Heading 2</h5><img src="http://placehold.it/200x200"></div>
		<div><h5>Heading 3</h5><img src="http://placehold.it/200x200"></div>
		<div><h5>Heading 4</h5><img src="http://placehold.it/200x200"></div>
		<div><h5>Heading 5</h5><img src="http://placehold.it/200x200"></div>
		<div><h5>Heading 6</h5><img src="http://placehold.it/200x200"></div>
		<div><h5>Heading 7</h5><img src="http://placehold.it/200x200"></div>
		<div><h5>Heading 8</h5><img src="http://placehold.it/200x200"></div>
		<div><h5>Heading 9</h5><img src="http://placehold.it/200x200"></div>
	</div>
</div>

<div class="store_info">
	<div class="contact1">
		<div class="heading">
			<img src="http://placehold.it/50x50">
			<span style="position:relative;bottom:15px;">Contact 1</span><br>
			<a href="#">Location finder</a>
		</div>
		<div class="address">
			<h3>Address</h3>
			<p>Address line 1</p>
			<p>Address line 2</p>
			<p><a href="#">Link1</a></p>
			<p><a href="#">Link2</a></p>
		</div>
	</div>
	<div class="contact2">
		<div class="heading">
			<img src="http://placehold.it/50x50">
			<span style="position:relative;bottom:15px;">Contact 2</span><br>
			<a href="#">Location finder</a>
		</div>
		<div class="address">
			<h3>Address</h3>
			<p>Address line 1</p>
			<p>Address line 2</p>
			<p><a href="#">Link1</a></p>
			<p><a href="#">Link2</a></p>
		</div>
	</div> 
</div>

	<div id="main_content">
		
	</div> -->
	<footer>
          <center><p><a href="http://www.sathiyasainathan.com/">Sathiyasainathan</a> &copy; Copyrights Reserved 2015-2019. </p><br></center>
          <a href="#">Back to top</a>
        </footer>
</body>
</html>