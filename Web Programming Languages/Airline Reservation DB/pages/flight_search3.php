<?php

session_start();
// if(isset($_SESSION['name'])){
// 	header("location: ../index.php");
// 	exit();
// }

$msg="";
$value1="";
$value2="";
if (isset($_POST['flight_number'])) {
	$flight_number = $_POST['flight_number'];
	$date = $_POST['date'];
	///////////////////////////////////
	$flight_number = strip_tags($flight_number);
	$date = strip_tags($date);
	///////////////////////////////////
	$flight_number = stripslashes($flight_number);
	$date = stripslashes($date);
	//////////////////////////////////
	if ((!$flight_number) || (!$date)) {
		$msg = "<p style='text-align: center; padding: 5px; color: #f00; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>How do you expect me to show result for NO input!!!<br> Please fill all the fields.</p>";
	} else {
	include_once("../scripts/connect.php");
	$sql = mysql_query("SELECT * FROM seat_reservation s WHERE s.flight_number='$flight_number' and s.DATE='$date' ");
	$count = mysql_num_rows($sql);
	if ($count > 0) {
		while ($row = mysql_fetch_array($sql)) {
			$flight_number = $row["flight_number"];
			$date = $row["DATE"];
			$seat_number = $row["SEAT_NUMBER"];
			$customer_name = $row["CUSTOMER_NAME"];
			$customer_phone = $row["CUSTOMER_PHONE"];
			// $weekdays = $row["weekdays"];
			// $value1 = "<p style='padding-top: 10px; color: #000; font-weight: bold; font-family: arial; font-size: 16px;'>Flight Details</p>";
			$value1 = "<table style='background: #000;' width='550' cellpadding='0' cellspacing='0' border='2'>
			<tr valign='top'>
					<td width='100'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Flight No.</p></td>
					<td width='100'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Date</p></td>
					<td width='100'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Seat No.</p></td>
					<td width='100'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Name</p></td>
					<td width='150'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Phone</p></td>
			</tr></table>";
			// $value2 = "Weekday";
			// $msg .= "<p style='padding-top: 10px; color: #333; font-weight: bold; font-family: arial; font-size: 16px;'>$flight_number | $weekdays <br/></p>";
			$msg .= "<table style='background: #333;' width='550' cellpadding='0' cellspacing='0' border='2'>
				<tr valign='top'>
					<td width='100'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 8px;'>$flight_number</p></td>
					<td width='100'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 8px;'>$date</p></td>
					<td width='100'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 8px;'>$seat_number</p></td>
					<td width='100'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 8px;'>$customer_name</p></td>
					<td width='150'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 8px;'>$customer_phone</p></td>
				</tr>
			</table>";
		}
	} else {
		$msg = "<p style='text-align: center; padding: 5px; color: #f00; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>Oops!!! It's an invalid Flight information, I can't retrive the result!<br> Try with a valid information.</p>";
	}
}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<title>Flight Search</title>
	<!-- <link rel="stylesheet" href="css/normalize.css"> -->
	<link rel="stylesheet" href="../css/white_black.css">
	<link rel="stylesheet" href="../admin/css/forms.css">
	<link href='http://fonts.googleapis.com/css?family=Changa+One|Open+Sans:400italic,700italic,400,700,800' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="../img/logo.jpg">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<div id="main_wrapper">
		<header id="main_header">
			<table width="970" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td align="right">
						<a href="index.php">
							<img src="../img/ta.jpg" alt="" width="120" height="120" border="0">
						</a>
					</td>
					<td align="left">
						<a href="index.php">
						<h1>Texas Airlines</h1></a><br/>
						<h2>Getting High Is My Job!</h2>
					</td>
				</tr>
			</table>
		</header>
		<nav id="main_menu">
			<a href="../index.php">
				<div id="page">Home</div>
			</a>
			<a href="flights.php">
				<div id="page">Flights</div>
			</a>
			<!-- <a href="#">
				<div id="page">Booking</div>
			</a> -->
			<a href="offers.php">
				<div id="page">Flight Deals</div>
			</a>
			<a href="about.php">
				<div id="page">About Us</div>
			</a>
			<a href="contact.php">
				<div id="page">Contact Us</div>
			</a>
		</nav>
		<div id="content_wrapper">
			<table cellpadding="0" cellspacing="0" border="0" width="1000">
				<tr>
					<td valign="top">
						<aside id="left_side">
							<div id="selected_tickets" style="width: 50px;">
									<a href="flight_search34.php">Back</a>
								</div>
						</aside>
					</td>
					<td valign="top">
						<section id="main_content">
							
							<br>
							<form method="post" action="flight_search3.php" enctype="multipart/form-data">
								<table width="100%" cellpaddin="3" cellspacing="3" border="0">
									<tr>
										<td align="left" font-weight="bold"><label style="font-family: 'PT Sans'; font-style: italic; font-size: 18px; color: #948760">Flight Number</label></td>
										<td align="left"><input type="text" name="flight_number" class="text_input" maxlength="50"></td>
									</tr>
									<tr>
										<td align="left" font-weight="bold"><label style="font-family: 'PT Sans'; font-style: italic; font-size: 18px; color: #948760">Date</label></td>
										<td align="left"><input type="date" name="date"></td>
									</tr>
									<tr>
										<br>
									</tr>
									<tr>
										<td align="center" colspan="5" font-weight="bold"><input type="submit" name="submit" id="button" value="Search"></td>
									</tr>
									<tr>
										<td align="center" colspan="5" font-weight="bold"><?php echo $value1; ?></td>
									</tr>
									<tr>
										<td align="center" colspan="5" font-weight="bold"><?php echo $msg; ?></td>
									</tr>
								</table>
							</form>
						</section>
					</td>
					<td valign="top">
						<aside id="right_side">
							<!-- id=cart_wrap -->
							<div id="booking_wrap">
								<!-- id=cart_header -->
								<div id="selected_tickets">
									<a href="../admin.php">Log In or Register</a>
								</div>
								<!-- id=cart_body -->
								<div id="booked_tickets">
								</div>
							</div>
						</aside>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<footer id="main_footer">
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="5">
			<tr>
				<td></td>
				<td><img src="../img/American-Express-icon.png" width="100" height="100"></td>
				<td><img src="../img/Discover-icon.png" width="100" height="100"></td>
				<td><img src="../img/paypal-icon.png" width="100" height="100"></td>
				<td><img src="../img/visa.png" width="100" height="100"></td>
				<td></td>
			</tr>
			<tr>
				<td align="center" colspan="6">
					<p>Copyright &copy; Texas Airlines 2015. All rights reserved. Designed by<a href="http://www.sathiyasainathan.com">Sathya.</a></p>
				</td>
			</tr>
		</table>
	</footer>>
</body>
</html>