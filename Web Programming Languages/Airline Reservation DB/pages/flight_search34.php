<?php

session_start();
// if(isset($_SESSION['name'])){
// 	header("location: ../index.php");
// 	exit();
// }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<title>Flight Search</title>
	<!-- <link rel="stylesheet" href="css/normalize.css"> -->
	<link rel="stylesheet" href="../css/white_black.css">
	<link rel="stylesheet" href="../admin/css/forms.css">
	<link href='http://fonts.googleapis.com/css?family=Changa+One|Open+Sans:400italic,700italic,400,700,800' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="../img/logo.jpg">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<div id="main_wrapper">
		<header id="main_header">
			<table width="970" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td align="right">
						<a href="index.php">
							<img src="../img/ta.jpg" alt="" width="120" height="120" border="0">
						</a>
					</td>
					<td align="left">
						<a href="index.php">
						<h1>Texas Airlines</h1></a><br/>
						<h2>Getting High Is My Job!</h2>
					</td>
				</tr>
			</table>
		</header>
		<nav id="main_menu">
			<a href="../index.php">
				<div id="page">Home</div>
			</a>
			<a href="flights.php">
				<div id="page">Flights</div>
			</a>
			<!-- <a href="#">
				<div id="page">Booking</div>
			</a> -->
			<a href="offers.php">
				<div id="page">Flight Deals</div>
			</a>
			<a href="about.php">
				<div id="page">About Us</div>
			</a>
			<a href="contact.php">
				<div id="page">Contact Us</div>
			</a>
		</nav>
		<div id="content_wrapper">
			<table cellpadding="0" cellspacing="0" border="0" width="1000">
				<tr>
					<td valign="top">
						<aside id="left_side">
							<div id="selected_tickets" style="width: 50px;">
									<a href="flights.php">Back</a>
								</div>
						</aside>
					</td>
					<td valign="top">
						<section id="main_content">
							
						</section>
					</td>
					<td valign="top">
						<aside id="right_side">
							<!-- id=cart_wrap -->
							<div id="booking_wrap">
								<!-- id=cart_header -->
								<div id="selected_tickets">
									<a href="../admin.php">Log In or Register</a>
								</div>
								<!-- id=cart_body -->
								<div id="booked_tickets">
								</div>
							</div>
						</aside>
					</td>
				</tr>				
			</table>
			<center>
			<table cellpadding="0" cellspacing="0" border="0" width="550">
				<tr valign="top">
					<td><center><p style="font-family: 'PT Sans'; font-style: italic; font-size: 25px; color: #948760"><b>SEARCH PASSANGERS MANIFEST HERE!</b></p></center></td>
				</tr>
				<tr>
					<td valign="top">
						<section id="main_content">
							<center><a href="flight_search3.php"><button style="width: 275px; box-shadow: 3px 3px 3px #000; font-family: 'PT Sans'; font-style: italic; background: #346; border-radius: 8px; padding: 5px; color: #000; font-size: 18px; font-weight: bold;">Using Flight Number and Date</button></a></center>
						</section>
					</td>
					<br>
				</tr>
				<tr>
					<td valign="top">
						<section id="main_content">
							<center><a href="flight_search4.php"><button style="width: 275px; box-shadow: 3px 3px 3px #000; font-family: 'PT Sans'; font-style: italic; background: #346; border-radius: 8px; padding: 5px; color: #000; font-size: 18px; font-weight: bold;">Using Passangers Name</button></a></center>
						</section>
					</td>
					<br>
				</tr>
			</table>
			</center>
		</div>
	</div>
	<footer id="main_footer">
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="5">
			<tr>
				<td></td>
				<td><img src="../img/American-Express-icon.png" width="100" height="100"></td>
				<td><img src="../img/Discover-icon.png" width="100" height="100"></td>
				<td><img src="../img/paypal-icon.png" width="100" height="100"></td>
				<td><img src="../img/visa.png" width="100" height="100"></td>
				<td></td>
			</tr>
			<tr>
				<td align="center" colspan="6">
					<p>Copyright &copy; Texas Airlines 2015. All rights reserved. Designed by<a href="http://www.sathiyasainathan.com">Sathya.</a></p>
				</td>
			</tr>
		</table>
	</footer>>
</body>
</html>