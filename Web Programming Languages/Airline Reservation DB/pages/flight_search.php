<?php

session_start();
// if(isset($_SESSION['name'])){
// 	header("location: ../index.php");
// 	exit();
// }

$msg="";
$value1="";
$value2="";
if (isset($_POST['departure_airport_code'])) {
	$departure_airport_code = $_POST['departure_airport_code'];
	$arrival_airport_code = $_POST['arrival_airport_code'];
	///////////////////////////////////
	$departure_airport_code = strip_tags($departure_airport_code);
	$arrival_airport_code = strip_tags($arrival_airport_code);
	///////////////////////////////////
	$departure_airport_code = stripslashes($departure_airport_code);
	$arrival_airport_code = stripslashes($arrival_airport_code);
	//////////////////////////////////
	if ((!$departure_airport_code) || (!$arrival_airport_code)) {
		$msg = "<p style='text-align: center; padding: 5px; color: #f00; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>How do you expect me to show result for NO input!!!<br> Please fill all the fields.</p>";
	} else {
	include_once("../scripts/connect.php");
	$choice = $_POST["result"];

	try{
        if($choice=="0"){
          $sql = mysql_query("SELECT flight_number,weekdays,departure_airport_code,arrival_airport_code FROM flight
          	WHERE departure_airport_code='$departure_airport_code'
          	AND arrival_airport_code='$arrival_airport_code'");
          $count = mysql_num_rows($sql);
			if ($count > 0) {
				while ($row = mysql_fetch_array($sql)) {
					$flight_number = $row["flight_number"];
					$weekdays = $row["weekdays"];
					$departure_airport_code = $row["departure_airport_code"];
					$arrival_airport_code = $row["arrival_airport_code"];
					switch ($weekdays) {
					    case 'Sun_Mon_Tue_Wed_Thu_Fri_Sat':
					        $weekdays = "All 7 Days";
					        break;
					    case 'Mon_Wed_Thu_Fri':
					        $weekdays = "MON, WED, THU, FRI";
					        break;
					}
					$value1 = "<table style='background: #000;' width='500' cellpadding='0' cellspacing='0' border='1'>
					<tr valign='top'>
							<td width='130'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Flight Number</p></td>
							<td width='185'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Weekdays</p></td>
							<td width='185'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Route</p></td>
						</tr></table>";
					$msg .= "<table style='background: #333; width='700' cellpadding='0' cellspacing='0' border='1'>
						<tr valign='top'>
							<td width='120'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 10px;'>$flight_number<br/></p></td>
							<td width='190'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 10px;'>$weekdays<br/></p></td>
							<td width='190'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 10px;'>$departure_airport_code &#8594 $arrival_airport_code<br/></p></td>
						</tr>
					</table>";
				}
			} else {
				$msg = "<p style='text-align: center; padding: 5px; color: #f00; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>Oopss!!! We don't have any flights for this Departure Code and Arrival Code.<br> Please enter a valid Code!</p>";
			}
        }
        elseif ($choice=="1"){
          	$sql = mysql_query("SELECT A.Flight_number flight_number,A.Airline,A.Departure_airport_code fstart,A.Scheduled_departure_time,A.Arrival_airport_code fconn,A.Scheduled_arrival_time,B.Flight_number,B.Scheduled_departure_time,B.Arrival_airport_code fend,B.Scheduled_arrival_time,A.WeekDays weekdays
          	FROM FLIGHT A INNER JOIN Flight B ON A.Arrival_airport_code=B.Departure_airport_code
          	-- WHERE A.Scheduled_arrival_time-B.Scheduled_departure_time<2
          	WHERE B.Scheduled_departure_time-A.Scheduled_arrival_time>1
          	AND A.Airline=B.Airline
          	AND A.Departure_airport_code='$departure_airport_code'
          	AND B.Arrival_airport_code='$arrival_airport_code'");
          	$count = mysql_num_rows($sql);
			if ($count > 0) {
				while ($row = mysql_fetch_array($sql)) {
					$flight_number = $row["flight_number"];
					$weekdays = $row["weekdays"];
					$fstart = $row["fstart"];
					$fconn = $row["fconn"];
					$fend = $row["fend"];
					switch ($weekdays) {
					    case 'Sun_Mon_Tue_Wed_Thu_Fri_Sat':
					        $weekdays = "All 7 Days";
					        break;
					    case 'Mon_Wed_Thu_Fri':
					        $weekdays = "MON, WED, THU, FRI";
					        break;
					}
					$value1 = "<table style='background: #000;' width='500' cellpadding='0' cellspacing='0' border='1'>
					<tr valign='top'>
							<td width='130'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Flight Number</p></td>
							<td width='185'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Weekdays</p></td>
							<td width='185'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Route</p></td>
						</tr></table>";
					$msg .= "<table style='background: #333; width='500' cellpadding='0' cellspacing='0' border='1'>
						<tr valign='top'>
							<td width='120'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 10px;'>$flight_number<br/></p></td>
							<td width='190'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 10px;'>$weekdays<br/></p></td>
							<td width='190'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 10px;'>$fstart &#8594 $fconn &#8594 $fend<br/></p></td>
						</tr>
					</table>";
				}
			} else {
				$msg = "<p style='text-align: center; padding: 5px; color: #f00; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>Oopss!!! We don't have any flights for this Departure Code and Arrival Code.<br> Please enter a valid Code!</p>";
			}
        }
        elseif ($choice=="2") {
        	$sql = mysql_query("SELECT A.Flight_number flight_number,A.Airline,A.Departure_airport_code fstart,A.Scheduled_departure_time,A.Arrival_airport_code fconn1,B.Arrival_airport_code fconn2,C.Arrival_airport_code fend,A.WeekDays weekdays
          	FROM FLIGHT A INNER JOIN Flight B ON A.Arrival_airport_code=B.Departure_airport_code
          	INNER JOIN Flight C ON B.Arrival_airport_code=C.Departure_airport_code
          	WHERE B.Scheduled_departure_time-A.Scheduled_arrival_time>1
          	AND C.Scheduled_departure_time-B.Scheduled_arrival_time>1
          	AND A.Arrival_airport_code <> 'ATL'
			AND B.Arrival_airport_code <> 'ATL'
          	AND A.Airline=B.Airline
          	AND B.Airline=C.Airline
          	AND A.Departure_airport_code='$departure_airport_code'
          	AND C.Arrival_airport_code='$arrival_airport_code'");
          	$count = mysql_num_rows($sql);
			if ($count > 0) {
				while ($row = mysql_fetch_array($sql)) {
					$flight_number = $row["flight_number"];
					$weekdays = $row["weekdays"];
					$fstart = $row["fstart"];
					$fconn1 = $row["fconn1"];
					$fconn2 = $row["fconn2"];
					$fend = $row["fend"];
					switch ($weekdays) {
					    case 'Sun_Mon_Tue_Wed_Thu_Fri_Sat':
					        $weekdays = "All 7 Days";
					        break;
					    case 'Mon_Wed_Thu_Fri':
					        $weekdays = "MON, WED, THU, FRI";
					        break;
					}
					$value1 = "<table style='background: #000;' width='500' cellpadding='0' cellspacing='0' border='1'>
					<tr valign='top'>
							<td width='130'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Flight Number</p></td>
							<td width='185'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Weekdays</p></td>
							<td width='185'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Route</p></td>
						</tr></table>";
					$msg .= "<table style='background: #333; width='500' cellpadding='0' cellspacing='0' border='1'>
						<tr valign='top'>
							<td width='100'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 10px;'>$flight_number<br/></p></td>
							<td width='190'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 10px;'>$weekdays<br/></p></td>
							<td width='210'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 10px;'>$fstart &#8594 $fconn1 &#8594 $fconn2 &#8594 $fend<br/></p></td>
						</tr>
					</table>";
				}
			} else {
				$msg = "<p style='text-align: center; padding: 5px; color: #f00; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>Oopss!!! We don't have any flights for this Departure Code and Arrival Code.<br> Please enter a valid Code!</p>";
			}
        }
        elseif ($choice=="3") {
        	$sql = mysql_query("SELECT A.Flight_number flight_number,A.Airline,A.Departure_airport_code fstart,A.Scheduled_departure_time,A.Arrival_airport_code fconn1,B.Arrival_airport_code fconn2,C.Arrival_airport_code fconn3,D.Arrival_airport_code fend,A.WeekDays weekdays
          	FROM FLIGHT A INNER JOIN Flight B ON A.Arrival_airport_code=B.Departure_airport_code
          	INNER JOIN Flight C ON B.Arrival_airport_code=C.Departure_airport_code
          	INNER JOIN Flight D ON C.Arrival_airport_code=D.Departure_airport_code
          	WHERE B.Scheduled_departure_time-A.Scheduled_arrival_time>1
          	AND C.Scheduled_departure_time-B.Scheduled_arrival_time>1
          	AND D.Scheduled_departure_time-C.Scheduled_arrival_time>1
          	AND A.Arrival_airport_code <> 'ATL'
			AND B.Arrival_airport_code <> 'ATL'
			AND C.Arrival_airport_code <> 'ATL'
          	AND A.Airline=B.Airline
          	AND B.Airline=C.Airline
          	AND C.Airline=D.Airline
          	AND A.Departure_airport_code='$departure_airport_code'
          	AND D.Arrival_airport_code='$arrival_airport_code'");
          	$count = mysql_num_rows($sql);
			if ($count > 0) {
				while ($row = mysql_fetch_array($sql)) {
					$flight_number = $row["flight_number"];
					$weekdays = $row["weekdays"];
					$fstart = $row["fstart"];
					$fconn1 = $row["fconn1"];
					$fconn2 = $row["fconn2"];
					$fconn3 = $row["fconn3"];
					$fend = $row["fend"];
					switch ($weekdays) {
					    case 'Sun_Mon_Tue_Wed_Thu_Fri_Sat':
					        $weekdays = "All 7 Days";
					        break;
					    case 'Mon_Wed_Thu_Fri':
					        $weekdays = "MON, WED, THU, FRI";
					        break;
					}
					$value1 = "<table style='background: #000;' width='500' cellpadding='0' cellspacing='0' border='1'>
					<tr valign='top'>
							<td width='130'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Flight Number</p></td>
							<td width='185'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Weekdays</p></td>
							<td width='185'><p style='text-align: center; padding: 5px; color: #e1e1e1; font-style: italic; font-weight: bold; font-family: 'PT Sans'; font-size: 14px;'>Route</p></td>
						</tr></table>";
					$msg .= "<table style='background: #333; width='600' cellpadding='0' cellspacing='0' border='1'>
						<tr valign='top'>
							<td width='130'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 10px;'>$flight_number<br/></p></td>
							<td width='185'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 10px;'>$weekdays<br/></p></td>
							<td width='285'><p style='text-align: center; padding: 5px; color: #fff; font-style: italic; font-family: 'PT Sans'; font-size: 10px;'>$fstart &#8594 $fconn1 &#8594 $fconn2 &#8594 $fconn3 &#8594 $fend<br/></p></td>
						</tr>
					</table>";
				}
			} else {
				$msg = "<p style='text-align: center; padding: 5px; color: #f00; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>Oopss!!! We don't have any flights for this Departure Code and Arrival Code.<br> Please enter a valid Code!</p>";
			}
        }
        }
        // throw exception incase of error
      catch(exception $e){
          echo "Failed to execute query!";
        }
}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<title>Flight Search</title>
	<link rel="stylesheet" href="../css/white_black.css">
	<link rel="stylesheet" href="../admin/css/forms.css">
	<link href='http://fonts.googleapis.com/css?family=Changa+One|Open+Sans:400italic,700italic,400,700,800' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="../img/logo.jpg">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<div id="main_wrapper">
		<header id="main_header">
			<table width="970" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td align="right">
						<a href="index.php">
							<img src="../img/ta.jpg" alt="" width="120" height="120" border="0">
						</a>
					</td>
					<td align="left">
						<a href="index.php">
						<h1>Texas Airlines</h1></a><br/>
						<h2>Getting High Is My Job!</h2>
					</td>
				</tr>
			</table>
		</header>
		<nav id="main_menu">
			<a href="../index.php">
				<div id="page">Home</div>
			</a>
			<a href="flights.php">
				<div id="page">Flights</div>
			</a>
			<!-- <a href="#">
				<div id="page">Booking</div>
			</a> -->
			<a href="offers.php">
				<div id="page">Flight Deals</div>
			</a>
			<a href="about.php">
				<div id="page">About Us</div>
			</a>
			<a href="contact.php">
				<div id="page">Contact Us</div>
			</a>
		</nav>
		<div id="content_wrapper">
			<table cellpadding="0" cellspacing="0" border="0" width="1000">
				<tr>
					<td valign="top">
						<aside id="left_side">
							<div id="selected_tickets" style="width: 50px;">
									<a href="flights.php">Back</a>
								</div>
						</aside>
					</td>
					<td valign="top">
						<section id="main_content">
							
							<br>
							<form method="post" action="flight_search.php" enctype="multipart/form-data">
								<div id="nav">
								<table lass="paginated" width="100%" cellpaddin="3" cellspacing="3" border="0">
									<tr>
										<td align="left" font-weight="bold"><label style="font-family: 'PT Sans'; font-style: italic; font-size: 18px; color: #948760">Number of Connections</label></td>
										<td align="left">
											<select name="result">
												<option value="0">0</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
											</select>
										</td>
									</tr>
									<tr>
										<td align="left"><p><label style="font-family: 'PT Sans'; font-style: italic; font-size: 18px; color: #948760">Destination Airport Code</label></td>
										<td align="left">
											<?php
											$query = "SELECT arrival_airport_code from flight GROUP BY arrival_airport_code";
											include_once("../scripts/connect.php");
											$result = mysql_query($query);
											echo "<select name='arrival_airport_code'>";
											echo "<option value='0'>-Select-</option>";
											while($row = mysql_fetch_array($result)) {
											    echo "<option value='".$row['arrival_airport_code']."'>".$row['arrival_airport_code']."</option>";
											}
											echo "</select>";
											?>
										</td>
									</tr>
									<tr>
										<td align="left"><p><label style="font-family: 'PT Sans'; font-style: italic; font-size: 18px; color: #948760">Source Airport Code</label></td>
										<td align="left">
											<?php
											$query = "SELECT departure_airport_code from flight GROUP BY departure_airport_code";
											include_once("../scripts/connect.php");
											$result = mysql_query($query);
											echo "<select name='departure_airport_code'>";
											echo "<option value='0'>-Select-</option>";
											while($row = mysql_fetch_array($result)) {
											    echo "<option value='".$row['departure_airport_code']."'>".$row['departure_airport_code']."</option>";
											}
											echo "</select>";
											?>
										</td>
									</tr>
									<!-- <tr>
										<td align="left" font-weight="bold"><label>Departure Airport Code</label></td>
										<td align="left"><input type="text" name="departure_airport_code" class="text_input" maxlength="50"></td>
									</tr> -->
									<!-- <tr>
										<td align="left" font-weight="bold"><label>Arrival Airport Code</label></td>
										<td align="left"><input type="text" name="arrival_airport_code" class="text_input" maxlength="50"></td>
									</tr> -->
									<tr>
										<br>
									</tr>
									<tr>
										<td align="center" colspan="5" font-weight="bold"><input type="submit" name="submit" id="button" value="Search"></td>
									</tr>
									<tr>
										<td align="center" colspan="5" font-weight="bold"><?php echo $value1; ?></td>
									</tr>
									<tr>
										<td align="center" colspan="5" font-weight="bold"><?php echo $msg; ?></td>
									</tr>
								</table>
								</div>

							</form>
						</section>
					</td>
					<td valign="top">
						<aside id="right_side">
							<!-- id=cart_wrap -->
							<div id="booking_wrap">
								<!-- id=cart_header -->
								<div id="selected_tickets">
									<a href="../admin.php">Log In or Register</a>
								</div>
								<!-- id=cart_body -->
								<div id="booked_tickets">
								</div>
							</div>
						</aside>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!-- <footer id="main_footer">
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="5">
			<tr>
				<td></td>
				<td><img src="../img/American-Express-icon.png" width="100" height="100"></td>
				<td><img src="../img/Discover-icon.png" width="100" height="100"></td>
				<td><img src="../img/paypal-icon.png" width="100" height="100"></td>
				<td><img src="../img/visa.png" width="100" height="100"></td>
				<td></td>
			</tr>
			<tr>
				<td align="center" colspan="6">
					<p>Copyright &copy; Texas Airlines 2015. All rights reserved. <a href="privacy.php">Privacy Policy.</a> <a href="terms.php">Terms & Conditions.</a></p>
				</td>
			</tr>
		</table>
	</footer> -->
</body>
</html>