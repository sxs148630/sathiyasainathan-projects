<?php

session_start();
$msg = "";
if (isset($_POST['username'])) {
  $admin = $_POST['username'];
  $password = $_POST['password'];
  $admin = stripslashes($admin);
  $password = stripslashes($password);
  $admin = strip_tags($admin);
  $password = strip_tags($password);
  if ((!$admin) || (!$password)) {
    $msg = "<p style='text-align: center; padding: 5px; color: #f00; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>Please fill all fields!</p>";
  }else {
    $admin = mysql_real_escape_string($admin);
    $password = md5($password);
    include_once ("scripts/connect.php");
    $sql = mysql_query("SELECT * FROM admin WHERE name='$admin' AND password='$password' LIMIT 1");
    $count = mysql_num_rows($sql);
    if ($count > 0) {
      while ($row = mysql_fetch_array($sql)) {
        $s_id = $row['id'];
        $s_name = $row['name'];
        $s_pass = $row['password'];
        $_SESSION['id'] = $s_id;
        $_SESSION['name'] = $s_name;
        $_SESSION['password'] = $s_pass;
        mysql_query("UPDATE admin SET last_log=now() WHERE name='$s_name' LIMIT 1");
        header("location: admin/index.php");
      }
    }else {
      $msg = "<p style='text-align: center; padding: 5px; color: #f00; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>Wrong username or password!</p>";
    }
  }
}

?>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>User Login</title>
  <link rel="stylesheet" href="css/style.css">
  <link rel="shortcut icon" href="img/logo.jpg">
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
  <form method="post" action="admin.php" class="login" autocomplete="on">
    <p>
      <label for="login">Username:</label>
      <input type="text" name="username" id="login" placeholder="name@example.com">
    </p>

    <p>
      <label for="password">Password:</label>
      <input type="password" name="password" id="password" placeholder="*********">
    </p>

    <p class="login-submit">
      <button type="submit" name="submit" class="login-button">Login</button>
    </p>

    <p class="forgot-password"><a href="#">Forgot your password?</a></p>
    <p><?php echo $msg; ?></p>
  </form>

  <section class="about">
    <p class="about-links">
      <a href="index.php" target="_parent">Home</a>
      <a href="signup.php" target="_parent">New User?</a>
    </p>
    <p class="about-author">
      &copy; 2014&ndash;2015 <a href="index.php" target="_blank">Texas Airlines</a> -
      All rights reserved<br>
      Designed by <a href="http://www.sathiyasainathan.com" target="_blank">Sathya</a>
  </section>
</body>
</html>
