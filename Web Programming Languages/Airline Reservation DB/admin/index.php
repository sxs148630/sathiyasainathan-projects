<?php

session_start();
if(!isset($_SESSION['name'])){
	header("location: ../index.php");
	exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Welcome Home</title>
	<meta charset="UTF-8">
	<link href='http://fonts.googleapis.com/css?family=Changa+One|Open+Sans:400italic,700italic,400,700,800' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="img/logo.jpg">
	<link rel="stylesheet" type="text/css" href="../css/white_black.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/main.css" media="screen">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<div id="main_wrapper">
		<?php include_once 'templates/tmp_header.php'; ?>
		<?php include_once 'templates/tmp_nav.php'; ?>
		<section id="main_content">
			<br/>
			<div id="para_wrapper">
			<p><b>Welcome!</b><br/></p>
			<p><b>With hubs at Dallas and Houston, Texas Aviation Services (TAS) was a "local service" airline in Texas and surrounding states. <br>In August 1953 it operated scheduled flights to 36 airports from El Paso to Memphis. By May 1968 TAS flew to 48 U.S. <br>airports plus Monterrey, Tampico and Veracruz in Mexico. The airline then changed its name to Texas Airlines and continued <br>to grow.</b></p>
			<!-- <br>
			<p><b>When Texas Airlines was merged into <a href="http://en.wikipedia.org/wiki/Continental_Airlines">Continental Airlines</a> in 1982 it had grown to reach Baltimore, Colorado Springs, Denver, Fort Lauderdale, Hartford, Kansas City, Los Angeles, Mexico City, Milwaukee, Minneapolis/St. Paul, Omaha, Phoenix, St. Louis, Salt Lake City, Tucson and Washington D.C. with an all Douglas DC-9 jet fleet.</b></p> -->
		</div>
			<br/>
			<br/>
		</section>
		<?php include_once 'templates/tmp_main_aside.php'; ?>
	</div>
	<div>
		<?php include_once 'templates/tmp_footer.php'; ?>
	</div>
</body>
</html>