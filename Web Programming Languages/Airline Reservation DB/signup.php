<?php

session_start();
if(isset($_SESSION['admin'])){
  header("location: admin/index.php");
  exit();
}
$msg1 = "";
if (isset($_POST['username1'])) {
  $username1 = $_POST['username1'];
  $password1 = $_POST['password1'];
  $password2 = $_POST['password2'];
  $username1 = stripslashes($username1);
  $password1 = stripslashes($password1);
  $username1 = strip_tags($username1);
  $password1 = strip_tags($password1);
  if ((!$username1) || (!$password1)) {
    $msg1 = "<p style='text-align: center; padding: 5px; color: #f00; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>Please fill all fields!</p>";
  }
  elseif ($password1 != $password2) {
    $msg1 = "<p style='text-align: center; padding: 5px; color: #f00; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>Password does not match!</p>";
  }
  else {
    $username1 = mysql_real_escape_string($username1);
    $password1 = md5($password1);
    include_once ("scripts/connect.php");
    try{
      mysql_query("INSERT INTO admin VALUES('','$username1','$password1',now())");

        $msg1 = "<p style='text-align: center; padding: 5px; color: #865; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>Your Profile has been created. Please <a href='admin.php' target='_blank'>login</a> to continue.</p>";
      // header("location: admin/index.php"); 
    }
    catch (exception $e){
      $msg1 = "<p style='text-align: center; padding: 5px; color: #f00; font-style: italic; font-family: 'PT Sans'; font-size: 14px;'>Username already exist!</p>";
    }
  }
}

?>
<!DOCTYPE html>
<html>
  <head>  
    <title>Sign Up</title>
    <link href="css/style1.css" rel='stylesheet' type='text/css' />
    <link rel="shortcut icon" href="img/logo.jpg">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfonts-->
    <link href='http://fonts.googleapis.com/css?family=Lobster|Pacifico:400,700,300|Roboto:400,100,100italic,300,300italic,400italic,500italic,500' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,500,600,700,300' rel='stylesheet' type='text/css'>
    <!--webfonts-->
  </head>
  <body>  
      <!--start-login-form-->
        <div class="main">
            <div class="login-head">
              <!-- <h1>Texas Airlines</h1> -->
          </div>
          <div  class="wrap">
              <div class="Regisration">
                <div class="Regisration-head">
                  <h2><span></span>Register</h2>
               </div>
                <form action="signup.php" method="post" autocomplete="on" enctype="multipart/form-data">
                  <input type="text" autocomplete="off" placeholder="Full Name" >
                  <!-- <input type="text" value="Last Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Last Name';}" > -->
                  <input type="text" placeholder="Email Address" name="username1" >
                  <!-- <input type="text" placeholder="User Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'User Name';}" > -->
                <input type="password" placeholder="******" name="password1"  >
                <input type="password" placeholder="******" name="password2" >
                 <div class="Remember-me">
                <div class="p-container">
                <label class="checkbox"><input type="checkbox" name="checkbox" checked><i></i>I agree to the Terms and Conditions</label>
                <br>
                <div class ="clear"></div>
                <p><?php echo $msg1; ?></p>
              </div>
                         
                <div class="submit">
                  <input type="submit" value="Sign Me Up >" >
                </div>
                  <div class="clear"> </div>
                </div>
                      
              </form>
          </div>
        <section class="about">
    <p class="about-links">
      <a href="index.php" target="_parent">Home</a>
      <a href="admin.php" target="_parent">Existing User?</a>
    </p>
    <p class="about-author">
      &copy; 2014&ndash;2015 <a href="index.php" target="_blank">Texas Airlines</a> -
      All rights reserved<br>
      Designed by <a href="http://www.sathiyasainathan.com" target="_blank">Sathya</a>
  </section>
  </body>
</html>


