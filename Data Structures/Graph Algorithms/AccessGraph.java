
public class AccessGraph 
{
	public static void main (String args[])
	{
		int noofvertices = 8;
		String[] Vertices = new String[noofvertices];
		int[][] AdjMatrix = new int[noofvertices][noofvertices];
		Graphs G1 = new Graphs(noofvertices);
		int size =0;
		
		G1.setvertices("A"); G1.setvertices("B"); G1.setvertices("C"); G1.setvertices("D");
		G1.setvertices("E"); G1.setvertices("F"); G1.setvertices("G"); G1.setvertices("H");
		//G1.setvertices("F1");
		
		G1.setedges("A", "B"); G1.setedges("A", "C"); G1.setedges("B", "D"); G1.setedges("B", "E");
		G1.setedges("B", "F"); G1.setedges("C", "F"); G1.setedges("C", "G"); G1.setedges("G", "H");
		//G1.setedges("F", "F1");
		
		System.out.println("Vertices of Graph are as follow:");
		G1.printvertices();
		System.out.println();
		System.out.println("Edges of Graph are as follow:");
		G1.printgraph();
		System.out.println();
		G1.printmatrix();
		System.out.println();
		Vertices = G1.getvertices();
		AdjMatrix = G1.getadjmatrix();
		size = G1.getsize();
		System.out.println();
		System.out.println("Depth First Search is as follow");		
		DeapthFirstSearch D1 = new DeapthFirstSearch(noofvertices);
		D1.dfs(Vertices,AdjMatrix, size);
		System.out.println();
		System.out.println();
		System.out.println("Breath First Search is as follow");
		BreathFirstSearch B1 = new BreathFirstSearch(noofvertices);
		B1.BFS(Vertices,AdjMatrix, size);
	}
	

}
