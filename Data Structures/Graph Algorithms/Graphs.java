import java.util.*;

public class Graphs 
{
    private int NumVertices; 
	private String [] Vertices;
	private int NumEdge = 0;
	private int[][] Adjmatrix;
	
	private int size=0;
	public Graphs(int vertices)
	{
		 NumVertices = vertices;
		 Vertices = new String[NumVertices];
		 Adjmatrix = new int[NumVertices][NumVertices];
		 for(int i=0;i<NumVertices;i++)
		 {
			 for(int j=0;j<NumVertices;j++)
				 Adjmatrix[i][j]=0;
		 }
	}
	public void setvertices (String ver)
	{
		Vertices[size] = ver;
		size++;
	}
	public int getindex (String vertex)
	{
		String ver=null;
		//String ver= Vertices[index];
		for (int i=0; i<size;i++)
		{
			if(Vertices[i].equals(vertex))
		      return i;
		}
		return -1;
		
	}
	public String getvertex (int index)
	{
		
		String ver= Vertices[index];
		
		return ver;
		
	}
	
	public String[] getvertices ()
	{
		//String ver= Vertices[index];
		return Vertices;
	}
	
	public void setedges (String vertex1,String vertex2)
	{
		int i=0;
		int index1=0;
		int index2=0;
		for (i=0;i<size;i++)
		{
			if (Vertices[i].equals(vertex1))
				 index1 = i;
			if(Vertices[i].equals(vertex2))
				index2 =i;
			
		}
		Adjmatrix[index1][index2] = 1;
		Adjmatrix[index2][index1] = 1;
		NumEdge = NumEdge +2;
	}
	
	public int[][] getadjmatrix ()
	{
		return Adjmatrix;
	}
	
	public void printgraph()
	{
		for(int i=0;i<NumVertices;i++)
		 {
			 for(int j=0;j<NumVertices;j++)
			 {
				 if(Adjmatrix[i][j]==1)
				 {
					 System.out.println(Vertices[i] +"-----"+Vertices[j]);
				 }
			 }
				 
		 }
	}
	public void printvertices()
	{
		for (int i=0;i<size;i++)
		{
			
			System.out.print(Vertices[i] +",");
		}
		System.out.println();
	}
	 public int getsize()
     {
		 return size;
     }
	public void printmatrix()
	{
		for (int i=0;i<size;i++)
		{
			for(int j=0;j<size;j++)
			{
				System.out.printf(Adjmatrix[i][j]+" ");
								
			}
			System.out.println();
				
		}
	}
	
}//class
