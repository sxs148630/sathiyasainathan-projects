
public class QS_Main {


	public static void main(String... args) {
		QS_HW obj = new QS_HW();
		int array[] = {12, 19, 5, 7, 0, -6, 1, -1, 4, 2, 5, -3, 11, 7, 8};
		System.out.println("Quick Sort");
		System.out.println("");
		
		System.out.println("Original Array:");
		for (int i : array) {
			System.out.print(i+" ");
		}
		
		obj.quickSort(array, 0, array.length - 1);
		
		System.out.println("\n\nSorted Array:");
		for (int i : array) {
			System.out.print(i+" ");
		}
	}
	
}
