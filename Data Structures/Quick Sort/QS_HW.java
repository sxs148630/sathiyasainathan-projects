//Quick Sort

public class QS_HW {

	static void quickSort(int[] array, int first, int last) {
		if (first < last) {
			int splitPoint = divisionQs(array, first, last); //Find the splitPoint where the pivot will be placed (so avoid using pivot element for further recursions)

			//Once we find the splitPoint, we will call the quicksort method in the array
			//before and after the splitPoint. Note that the splitPoint will already be inplace once we find the divisionQs
			quickSort(array, first, splitPoint - 1);
			quickSort(array, splitPoint + 1, last);
		}
	}
	
	private static int divisionQs(int[] array, int first, int last) {
		int pivot = array[first]; //Let's choose pivot to be the first element always
		boolean done = false;
		int leftMark = first + 1, rightMark = last;
		
		while (!done) {
			while (leftMark <= rightMark && array[leftMark] <= pivot) {
				leftMark++; //Increment till you find the element that is > pivot and is in left array
			}
			while (rightMark >= leftMark && array[rightMark] >= pivot) {
				rightMark--; //Decrement till you find the element that is < pivot and is in right array
			}
			if (rightMark < leftMark) {
				done = true; //Once we've exhausted all the numbers and yet to find elements that are out of place, we are done
			}
			else { //Swap the numbers that are out of place
				int temp = array[leftMark];
				array[leftMark] = array[rightMark];
				array[rightMark] = temp;
			}
		}
		//Put the pivot back to where it belongs (i.e.) at the rightMark
		int temp = array[first];
		array[first] = array[rightMark];
		array[rightMark] =  temp;
		
		return rightMark; //The rightMark is the splitPoint and is place that holds the pivot at the end of every recursion
	}
}
