package assignment3;

public class HeapSortMain {
	public static void main(String[] args) {
	    int input[] = new int[]{5, 5, 10, 10, 3, 1, 9, 6, 11, 15, 8, 7, 75, 68, 99};
	    System.out.println("Input before sorting");
	    HeapSortImplementation.displayArrayElements(input);
	    HeapSortImplementation.heapSort(input);
	    System.out.println();
	    System.out.println("Input after sorting");
	    HeapSortImplementation.displayArrayElements(input);
	}

}
