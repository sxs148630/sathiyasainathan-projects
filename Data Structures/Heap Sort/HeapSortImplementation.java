package assignment3;

public class HeapSortImplementation {

	
	public static int size;
	
	public static void maxHeapBuilder(int input[])
	{
	    size = input.length;
	    for(int i=input.length/2; i>=0;i--)
	    {
	        performMaxHeapify(input, i);
	    }
	}
	public static void performMaxHeapify(int input[],int i)
	{
	    int leftNodeIndex=getLeftNodeIndex(i);
	    int rightNodeIndex=getRightNodeIndex(i);
	    int largestNumberIndex;
	    if(leftNodeIndex<size && input[leftNodeIndex]>input[i])
	    {
	        largestNumberIndex = leftNodeIndex;
	    } 
	    else 
	    {
	        largestNumberIndex=i;
	    }
	  
	    if(rightNodeIndex<size && input[rightNodeIndex]>input[largestNumberIndex])
	    {
	        largestNumberIndex = rightNodeIndex;
	    }
	 
	    if(largestNumberIndex!=i)
	    {
	        int temp = input[i];
	        input[i]=input[largestNumberIndex];
	        input[largestNumberIndex]=temp;
	        performMaxHeapify(input, largestNumberIndex);
	    }

	}
	public static int getLeftNodeIndex(int i)
	{
	    return (2*i)+1;
	}

	public static int getRightNodeIndex(int i)
	{
	    return (2*i)+2;
	}
	public static void heapSort(int input[])
	{
	    maxHeapBuilder(input);
	    for(int i=input.length-1;i>=1;i--)
	    {
	        int temp = input[0];
	        input[0]=input[i];
	        input[i]=temp;
	        size  = size-1;
	        performMaxHeapify(input,0);
	    }
	}
	
	public static void displayArrayElements(int input[])
	{
		for(int i=0;i<input.length;i++)
		{
			System.out.print(input[i]+"\t");
		}
	}

	}
