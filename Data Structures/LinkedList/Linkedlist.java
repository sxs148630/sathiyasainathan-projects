package LinkListSum;

class LinkedList {
	public LinkedList next;
	public int value;

	//constructor for the class
	public LinkedList(int value) {
		this.value = value;
		this.next = null;
	}

	//override inorder to print the data in list format
	@Override
	public String toString() {

		String data = "";
		LinkedList current = this;
		do {
			data += current.value + " ";
			current = current.next;
		} while (current != null);

		return data;
	}
}

package LinkListSum;

public class LinkedListSumRec {

	public static void main(String args[]) {

		//giving input data and forming a linked list structure
		LinkedList linkedList = new LinkedList(5);
		linkedList.next = new LinkedList(4);
		linkedList.next.next = new LinkedList(3);
		linkedList.next.next.next = new LinkedList(2);
		linkedList.next.next.next.next = new LinkedList(1);

		//printing the original list
		System.out.println("Input List: " + linkedList.toString());

		//recursive sum of the list
		int a = recursiveSum(linkedList);
		System.out.println("Sum of the elements: " + a);
		
		//reverse of the list
		linkedList = recursiveReverse(linkedList);
		System.out.println("Reversed List: " + linkedList.toString());

//		 iteratively reverse and print
		 linkedList = iterativeReverse(linkedList);
		 System.out.println("Iteratively Recursed to Original: "
		 + linkedList.toString());
	}

	/**
	 * This method uses recursive method to reverse a singly linked list.
	 */
	public static LinkedList recursiveReverse(LinkedList linkedList) {

		// check for empty or size 1 linked list. This is a base condition to
		// terminate recursion.
		if (linkedList == null || linkedList.next == null) {
			return linkedList;
		}

		LinkedList remainingReverse = recursiveReverse(linkedList.next);

		// update the tail as beginning
		LinkedList current = remainingReverse;
		while (current.next != null) {
			current = current.next;

		}
		// assign the head as a tail
		current.next = linkedList;
		linkedList.next = null;

		return remainingReverse;
	}

	private static int recursiveSum(LinkedList head) {
		if (head.next == null) {
			return head.value;
		}
		return head.value + recursiveSum(head.next);
	}

	public static LinkedList iterativeReverse(LinkedList linkedList) {

		if (linkedList == null || linkedList.next == null) {
			return linkedList;
		}

		LinkedList prevNode, currNode, nextNode;
		prevNode = null;
		nextNode = null;
		currNode = linkedList;

		while (currNode != null) {
			nextNode = currNode.next;
			currNode.next = prevNode;
			prevNode = currNode;
			currNode = nextNode;
		}

		return prevNode;
	}
}