package utd.persistentDataStore.datastoreServer.commands;

import java.io.IOException;

import utd.persistentDataStore.utils.FileUtil;
import utd.persistentDataStore.utils.ServerException;
import utd.persistentDataStore.utils.StreamUtil;

public class DeleteCommand extends ServerCommand {

	@Override
	public void run() throws IOException, ServerException {
		// TODO Auto-generated method stub
		
		try
		{
		String fileName = null;
		fileName = StreamUtil.readLine(this.inputStream);
		FileUtil.deleteData(fileName);
		sendOK();
		}catch(IOException e)
		{
			sendError(e.getMessage());
			throw e;
		}catch(ServerException e)
		{
			sendError(e.getMessage());
			throw e;
		}
		
	}

}
