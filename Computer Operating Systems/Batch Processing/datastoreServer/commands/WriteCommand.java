package utd.persistentDataStore.datastoreServer.commands;

import java.io.IOException;

import utd.persistentDataStore.utils.FileUtil;
import utd.persistentDataStore.utils.ServerException;
import utd.persistentDataStore.utils.StreamUtil;

public class WriteCommand extends ServerCommand {

	@Override
	public void run() throws IOException, ServerException {
		// TODO Auto-generated method stub
		
		try
		{
		String fileName = null;
		String dataSize = null;
		byte[] data = null;
		fileName = StreamUtil.readLine(this.inputStream);
		dataSize = StreamUtil.readLine(this.inputStream);
		data = StreamUtil.readData(Integer.parseInt(dataSize), this.inputStream);
		FileUtil.writeData(fileName, data);
		sendOK();
		}catch(IOException e)
		{
			sendError(e.getMessage());
			throw e;
		}catch(Exception e)
		{
			sendError(e.getMessage());
			throw e;
		}
		
	}
	
	
	
}
