package utd.persistentDataStore.datastoreServer.commands;

import java.io.IOException;

import utd.persistentDataStore.utils.FileUtil;
import utd.persistentDataStore.utils.ServerException;
import utd.persistentDataStore.utils.StreamUtil;

public class ReadCommand extends ServerCommand {

	@Override
	public void run() throws IOException, ServerException {
		// TODO Auto-generated method stub
		
		try
		{
		String fileName = null;
		byte[] data = null;
		Integer length = null;
		String fileLength = null;
		fileName = StreamUtil.readLine(this.inputStream);
		data = FileUtil.readData(fileName);
		length= data.length;
		fileLength = length.toString();
		sendOK();
		StreamUtil.writeLine(fileLength, this.outputStream);
		StreamUtil.writeData(data, this.outputStream);
		
		}catch(IOException e)
		{
			sendError(e.getMessage());
			throw e;
		}catch(ServerException e)
		{
			sendError(e.getMessage());
			throw e;
		}

}
}
