package utd.persistentDataStore.datastoreServer.commands;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import utd.persistentDataStore.utils.FileUtil;
import utd.persistentDataStore.utils.ServerException;
import utd.persistentDataStore.utils.StreamUtil;

public class DirectoryCommand extends ServerCommand {

	@Override
	public void run() throws IOException, ServerException {
		// TODO Auto-generated method stub
		List<String> fileList = null; 
		try
		{
		fileList =  FileUtil.directory();
		sendOK();
		if(fileList!=null)
		{
			Integer listSizeInt = fileList.size();
			String listSize = listSizeInt.toString();
			StreamUtil.writeLine(listSize, this.outputStream);
			for (Iterator<String> iterator = fileList.iterator(); iterator.hasNext();) {
				String fileName = (String) iterator.next();
				StreamUtil.writeLine(fileName, this.outputStream); 
			}
		}
		}catch(IOException e)
		{
			sendError(e.getMessage());
			throw e;
		}catch(ServerException e)
		{
			sendError(e.getMessage());
			throw e;
		}
		
	}

}
