package utd.persistentDataStore.datastoreClient;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import utd.persistentDataStore.utils.StreamUtil;

public class DatastoreClientImpl implements DatastoreClient {
	private static Logger logger = Logger.getLogger(DatastoreClientImpl.class);
	
	private InetAddress address;
	private int port;
	private Socket socket;
	public DatastoreClientImpl(InetAddress address, int port) {
		this.address = address;
		this.port = port;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * utd.persistentDataStore.datastoreClient.DatastoreClient#write(java.lang
	 * .String, byte[])
	 */
	@Override
	public void write(String name, byte data[]) throws ClientException {
		logger.debug("Executing Write Operation");
		try {
			
			socket = new Socket(this.address, this.port);
			Integer lengthOfData = data.length;
			String requestLine1 = "write";
			String requestLine2 = name;
			String requestLine3 = lengthOfData.toString();
			OutputStream outputStream = socket.getOutputStream();
			StreamUtil.writeLine(requestLine1, outputStream);
			StreamUtil.writeLine(requestLine2, outputStream);
			StreamUtil.writeLine(requestLine3, outputStream);
			StreamUtil.writeData(data, socket.getOutputStream());
			
		} catch (IOException e) {
			throw new ClientException(e.getMessage(), e);
		}catch(Exception e)
		{
			throw new ClientException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * utd.persistentDataStore.datastoreClient.DatastoreClient#read(java.lang
	 * .String)
	 */
	@Override
	public byte[] read(String name) throws ClientException {
		logger.debug("Executing Read Operation");
		String requestLine1 = null;
		String requestLine2 = null;
		String length = null;
		String response = null;
		byte[] data = null;
		try
		{
		socket = new Socket(this.address, this.port);
		requestLine1 = "read";
		requestLine2 = name;
		OutputStream outputStream = socket.getOutputStream();
		InputStream inputStream = socket.getInputStream();
		StreamUtil.writeLine(requestLine1, outputStream);
		StreamUtil.writeLine(requestLine2, outputStream);
		response = StreamUtil.readLine(inputStream);
		if(!response.equalsIgnoreCase("ok"))
		{
			throw new ClientException(response);
		}

		length = StreamUtil.readLine(inputStream);
		data = StreamUtil.readData(Integer.parseInt(length), inputStream);
		}catch(Exception e)
		{
			throw new ClientException(e.getMessage(), e);
		}
		
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * utd.persistentDataStore.datastoreClient.DatastoreClient#delete(java.lang
	 * .String)
	 */
	@Override
	public void delete(String name) throws ClientException {
		logger.debug("Executing Delete Operation");
		String requestLine1 = null;
		String requestLine2 = null;
		String response = null;
		try
		{
		socket = new Socket(this.address, this.port);
		requestLine1 = "delete";
		requestLine2 = name;
		OutputStream outputStream = socket.getOutputStream();
		InputStream inputStream = socket.getInputStream();
		StreamUtil.writeLine(requestLine1, outputStream);
		StreamUtil.writeLine(requestLine2, outputStream);
		response = StreamUtil.readLine(inputStream);
		if(!response.equalsIgnoreCase("ok"))
		{
			throw new ClientException(response);
		}
		}catch(Exception e)
		{
			throw new ClientException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see utd.persistentDataStore.datastoreClient.DatastoreClient#directory()
	 */
	@Override
	public List<String> directory() throws ClientException {
		logger.debug("Executing Directory Operation");
		String requestLine1 = null;
		Integer noOfFiles = null;
		List<String> listOfFiles = new ArrayList<String>();
		try
		{
		socket = new Socket(this.address, this.port);
		requestLine1 = "directory";
		OutputStream outputStream = socket.getOutputStream();
		InputStream inputStream = socket.getInputStream();
		StreamUtil.writeLine(requestLine1, outputStream);
		StreamUtil.readLine(inputStream);
		noOfFiles = Integer.parseInt(StreamUtil.readLine(inputStream));
		for (int i = 0; i < noOfFiles; i++) {
			String fileName = StreamUtil.readLine(inputStream);
			listOfFiles.add(fileName);
		}
		}catch(Exception e)
		{
			throw new ClientException(e.getMessage(), e);
		}
		return listOfFiles;
	}

}
