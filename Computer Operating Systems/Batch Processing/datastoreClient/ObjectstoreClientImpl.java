package utd.persistentDataStore.datastoreClient;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;

import org.apache.log4j.Logger;

public class ObjectstoreClientImpl extends DatastoreClientImpl implements ObjectstoreClient
{
	private static Logger logger = Logger.getLogger(ObjectstoreClientImpl.class);

	public ObjectstoreClientImpl(InetAddress address, int port)
	{
		super(address, port);
	}

    /* (non-Javadoc)
	 * @see utd.persistentDataStore.datastoreClient.ObjectstoreClient#writeObject(java.lang.String, java.lang.Object)
	 */
    @Override
    public void writeObject(String name, Object object) throws ClientException
    {
		logger.debug("Executing WriteObject Operation");
		
		try {
			 ByteArrayOutputStream bos = new ByteArrayOutputStream();
		     ObjectOutputStream oos = new ObjectOutputStream(bos);
		     oos.writeObject(object);
		     this.write(name, bos.toByteArray());
		} catch (IOException e) {
			throw new ClientException(e.getMessage(), e);
		} catch(Exception e)
		{
			throw new ClientException(e.getMessage(), e);
		}
		
    }
    
    /* (non-Javadoc)
	 * @see utd.persistentDataStore.datastoreClient.ObjectstoreClient#readObject(java.lang.String)
	 */
    @Override
    public Object readObject(String name) throws ClientException
    {
    	byte[] data = null;
    	Object object = null;
    	try
    	{
		logger.debug("Executing ReadObject Operation");
		data  =  this.read(name);
		ByteArrayInputStream bos = new ByteArrayInputStream(data);
        ObjectInputStream oos = new ObjectInputStream(bos);
        object = oos.readObject();
    	}catch(Exception e)
    	{
    		throw new ClientException(e.getMessage(), e);
    	}
		return object;
    }
    


}
