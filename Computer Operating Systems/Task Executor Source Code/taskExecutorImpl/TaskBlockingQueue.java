package edu.utdallas.taskExecutorImpl;

import java.util.LinkedList;
import java.util.List;

import edu.utdallas.taskExecutor.Task;

/**
 * This is the Implementation of blocking queue
 *
 */
public class TaskBlockingQueue {
	private List<Task> listOfTasks;
	
	/**
	 * Constructor which initializes the listOfTasks
	 */
	public TaskBlockingQueue() {
		this.listOfTasks = new LinkedList<Task>();
	}
	
	/**
	 * Method to add tasks in the queue
	 */
	public void add(Task task) {
		this.listOfTasks.add(task);
	}
	
	/**
	 * Method to retrieve tasks from the queue
	 */
	public Task retrieve() throws Exception {
		
		if(this.listOfTasks.isEmpty()) {
			throw new Exception();
		}
		
		Task task = this.listOfTasks.remove(0);
		System.out.println(Thread.currentThread().getName() + ": Taking task.");
		
		return task;
	}
	
	public int getSize() {
		return this.listOfTasks.size();
	}
}
