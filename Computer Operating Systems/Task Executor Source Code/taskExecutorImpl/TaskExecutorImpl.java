package edu.utdallas.taskExecutorImpl;
import edu.utdallas.taskExecutor.Task;
import edu.utdallas.taskExecutor.TaskExecutor;


public class TaskExecutorImpl implements TaskExecutor{
private final TaskBlockingQueue queueOfTasks;
	
	
	/**
	 * Constructor to set the thread pool size
	 */
	public TaskExecutorImpl(int poolSize) {
		// Initialize Tasks Queue
		this.queueOfTasks = new TaskBlockingQueue();
		
		new TaskRunner(poolSize, queueOfTasks);
	}

	/** 
	 * This method is to add a task to the task queue
	 * It will be called by the Testing application to add the tasks
	 */
	public void addTask(Task task) {
		Integer queueSize = queueOfTasks.getSize();	
		
		queueOfTasks.add(task);
		
		if(queueSize == 0) {
			System.out.println("Task Queue empty. Notify waiting thread.");
			synchronized(queueOfTasks) {
				queueOfTasks.notify();
			}
		}
	}


}
