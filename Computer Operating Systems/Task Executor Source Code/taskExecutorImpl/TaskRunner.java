package edu.utdallas.taskExecutorImpl;

import java.util.ArrayList;
import java.util.List;
import edu.utdallas.taskExecutor.Task;
import java.lang.Runnable;

public class TaskRunner {
	private int poolSize;
	private TaskBlockingQueue taskQueue;
	private List<Thread> threads;
	Object monitor = new Object();
	
	/**
	 * Constructor to run the loop
	 */
	public TaskRunner(int poolSize, TaskBlockingQueue taskQueue) {
		this.poolSize = poolSize;
		this.taskQueue = taskQueue;
		//Initializing the thread pool size
		this.threads = new ArrayList<Thread>(poolSize);
		
		this.runLoop();
	}
	
	public Integer getPoolSize() {
		return this.poolSize;
	}
	
	/** 
	 * This method is to Run or Block each thread in a pool and notifies the user
	 * when the thread is blocked due to empty queue. It is invoked in the Thread Constructor block
	 */
	public void runLoop() {
		for(int i = 0; i < this.poolSize; i++) {
			String threadName = "Thread" + i;
			System.out.println(threadName + ": Added.");
			Thread thread = new Thread(new Runnable(){
				//@Override
				public void run() {
					while(true) {
						synchronized(monitor){
							try {
								monitor.wait();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						synchronized(taskQueue) {
							try {
								
								Task task = taskQueue.retrieve();
								task.execute();
								} catch (Exception e) {
								System.out.println(Thread.currentThread().getName() + ": Task Queue is empty. Thread blocked.");
								try {
									taskQueue.wait();
								} catch (InterruptedException e1) {
									e1.printStackTrace();
								}
							}
						}
						synchronized(monitor)
						{
							monitor.notify();
						}
					}
					
				}
			});
			thread.setName(threadName);
			this.threads.add(thread);
			System.out.println(threadName + ": Starting.");
			thread.start();
		}
		synchronized(monitor){
			monitor.notify();
		}
	}
}
