import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;
import java.util.Map.Entry;

public class Token
{
	static int nod = 0;
	static int noto = 0;
	static int nou = 0;
	static int noo = 0;
	static HashMap<String,Integer> wds = new HashMap<String, Integer>();

	public static void main(String[] args) throws Exception 
	{
		if (args.length != 1)
		{
			System.out.println("Please provide the input path");
			return;
		}
		File inputFile = new File(args[0]);
		if(!inputFile.exists() || !inputFile.isDirectory())
		{
			System.out.println("Please provide the input path correctly");
			return;
		}
		for(File fl: inputFile.listFiles())
		{
			nod += 1;
			if (fl.isFile())
			{
				Scanner sc = new Scanner(fl);  
				while(sc.hasNext()) 
				{
					String wd = sc.next();
					wd = wd.replaceAll("[^a-zA-Z-']","").trim().toLowerCase();
					
					if(!wd.isEmpty())
					{
						AddToDictionary(wd);
					}
				}
				sc.close();
			 }
		}			
		for(Entry<String, Integer> item: wds.entrySet())
		{
			nou += 1;
			noto += item.getValue();
			if(item.getValue() == 1)
			{
				noo += 1;
			}
		}		
		TokenWords(wds);
	}

	private static void AddToDictionary(String token) 
	{
		
		 if (token.contains("-"))
		{
			String[] splitTokens = token.split("-");
			for(String t : splitTokens)
			{
				TknDictionary(t);
			}
		}		 
		else if (token.endsWith("'s"))
		{
			token = token.replace("'s", "");
			TknDictionary(token);	
		}
		else if (token.startsWith("\'"))
			{	
				token = token.substring(1, token.length());
	                        if (token.endsWith("\'"))
	                        {
	                              token = token.substring(0, token.length() - 1);
	                        }
				TknDictionary(token);
			}
		else if (token.endsWith("\'"))
			{	
				token = token.substring(0, token.length() - 1);
				TknDictionary(token);
			}
		else if(token.endsWith("\'s"))
		{			
			token = token.substring(0,token.length()-2);
			TknDictionary(token);
		}
		else if(token.endsWith("\'es"))
		{
			token = token.substring(0,token.length()-3);
			TknDictionary(token);
		}
		else if(token.contains("'"))
		{
			token = token.replaceAll("'", "");
			TknDictionary(token);
		}
		else 
		{
			TknDictionary(token);
		}		
	}

	private static void TknDictionary(String token) 
	{
		token = token.replaceAll("['.]+", "");
		if (wds.containsKey(token)) 
		{
			wds.put(token, wds.get(token) + 1);
		} 
		else 
		{
			wds.put(token, 1);
		}		
	}
	
	private static void TokenWords(Map<String, Integer> tokenmap)
	{
		class newComapartor implements Comparator<String>
		{
			Map<String, Integer> map;

			public newComapartor(Map<String, Integer> base) 
			{
				this.map = base;
			}

			public int compare(String firstvalue, String secondvalue) 
			{
				if (map.get(firstvalue) < map.get(secondvalue))
				{
					return 1;
				} 
				return -1;
			}
		}

		newComapartor vc =  new newComapartor(tokenmap);
		TreeMap<String,Integer> token = new TreeMap<String,Integer>(vc);
		token.putAll(tokenmap);
		Set<Entry<String, Integer>> es = token.entrySet();
		Iterator<Entry<String, Integer>> ite = es.iterator();
		while(ite.hasNext())
		{
			System.out.println(ite.next());

		}
	}
}
